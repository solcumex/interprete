/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EasyIDE;

/**
 *
 * @author DDC Programación, UCI
 */
import java.net.MalformedURLException;
import javax.swing.*;
import java.io.*;
import java.awt.*;
import java.util.LinkedList;
import java.util.LinkedList;
import EasyCompiler.SymbolsTable.*;
import EasyCompiler.LexicalAnalyzer.*;
import EasyCompiler.SintaxAnalyzer.*;
import EasyCompiler.Errors.*;
import EasyCompiler.Modelos.Operacion;
import EasyCompiler.Stream.*;
import java.util.ArrayList;

public class IDEManager {

    private MainFrame mainFrame;
    private DocumentInfo document;
    private static IDEManager instance = null;
    private LinkedList<String> scannerOutput;
    private ArrayList<Operacion> scannerOperacion;
    private String resultadoFinal="";
    private String erroresFinal="";

    private IDEManager() {
        mainFrame = new MainFrame();
        mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        document = new DocumentInfo();
        scannerOutput = new LinkedList<String>();
    }

    void sourceEditorTextChanged() {
        document.modified();
    }

    public MainFrame getMainFrame() {
        return mainFrame;
    }

    public static IDEManager getInstance() {
        if (instance == null) {
            instance = new IDEManager();
        }
        return instance;
    }

    
    public void newDocument() throws FileNotFoundException, IOException {
        if (!document.isSave()) {
            int option = JOptionPane.showConfirmDialog(mainFrame, "Save changes?", "Save", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_OPTION) {
                save();
            }
        }
        mainFrame.getSourceEditor().setText(null);
        mainFrame.getJTabbedPane1().setSelectedIndex(0);
        mainFrame.getConsole().setText(null);
        mainFrame.getScannerOutput().setText(null);
        document.newDocument();
    }

    private void saveSource(String filename) throws FileNotFoundException, IOException {
        FileOutputStream out = new FileOutputStream(filename);
        out.write(mainFrame.getSourceEditor().getText().getBytes());
        out.close();
        document.saveAs(filename);
    }

    public void save() throws FileNotFoundException, IOException {
        if (!document.isSave()) {
            if (document.getFilename().equals("")) {
                saveAs();
            } else {
                saveSource(document.getFilename());
            }
        }
    }

    public void saveAs() throws FileNotFoundException, IOException {
        JFileChooser chooser = new JFileChooser();
        int modalResult = chooser.showSaveDialog(mainFrame);

        if (modalResult == JFileChooser.APPROVE_OPTION) {
            saveSource(chooser.getSelectedFile().getName());
        }
    }

    public void open() throws FileNotFoundException, IOException {
        if (!document.isSave()) {
            int option = JOptionPane.showConfirmDialog(mainFrame, "Save changes?", "Save", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_OPTION) {
                save();
            }
        }

        JFileChooser chooser = new JFileChooser();
        int modalResult = chooser.showOpenDialog(mainFrame);
        if (modalResult == JFileChooser.APPROVE_OPTION) {
            mainFrame.getSourceEditor().setText(null);
            FileInputStream in = new FileInputStream(chooser.getSelectedFile());

            String text = new String("");
            int c = in.read();
            while (c != -1) {
                text = text + Character.toString((char) c);
                c = in.read();
            }

            mainFrame.getSourceEditor().setText(text);
            mainFrame.getJTabbedPane1().setSelectedIndex(0);
            mainFrame.getConsole().setText(null);
            mainFrame.getScannerOutput().setText(null);

            document.open(chooser.getSelectedFile().getName());
            in.close();

        }
    }

    public void exit() {
        mainFrame.dispose();
    }

    private void prepareToBuild() {
        mainFrame.getScannerOutput().setText(null);
        mainFrame.getJTabbedPane1().setSelectedIndex(1);
        mainFrame.getConsole().setText(null);
        mainFrame.getScannerOutput().setText(null);
    }

    private int line;

    public ArrayList<String> dividirTexto(String textos) {
        ArrayList<String> listadoDeTextoAux = new ArrayList<>();

        String[] cadenado = textos.split("</s>");

        if (cadenado.length == 1) {

            for (int i = 0; i < cadenado.length; i++) {

                listadoDeTextoAux.add("</s> " + cadenado[i] + " </s>");
            }
        } else {
            for (int i = 0; i < cadenado.length; i++) {

                listadoDeTextoAux.add(cadenado[i] + " </s>");
            }
        }

        return listadoDeTextoAux;

    }

    public void build() throws IOException, Exception {
        ArrayList<String> listadoDeTexto = new ArrayList<>();
        prepareToBuild();
        SymbolsTable symbolsTable = new SymbolsTable();
        ErrorReporter errorReporter = new ErrorReporter();

        String cadenaDeTexto = mainFrame.getSourceEditor().getText();
        listadoDeTexto = dividirTexto(cadenaDeTexto);

        for (int j = 0; j < listadoDeTexto.size(); j++) {

            
            if (!listadoDeTexto.get(j).contains("<s>")) {
                continue;
                
            }
            if (!listadoDeTexto.get(j).contains("<s>")) {
                continue;
                
            }
            Scanner scanner = new Scanner(new StringSourceStream(listadoDeTexto.get(j)), symbolsTable, errorReporter);
            //  Scanner scanner = new Scanner(new StringSourceStream(mainFrame.getSourceEditor().getText()), symbolsTable, errorReporter);
            Parser parser = new Parser(scanner, errorReporter);
            scannerOperacion = scanner.getOperaList();

            parser.Parse();

            LinkedList<Token> output = scanner.getCurrentOutput();
            int tokensCount = output.size();
            scannerOutput.clear();
            line = 1;
            //not included token EOT
            for (int i = 0; i < tokensCount - 1; i++) {
                addTokenToScannerOutput(output.get(i));
            }

          String texto=  writeScannerOutput();
 
            String text = "";
            int errorCount = errorReporter.size();
            if (errorCount > 0) {
                text = "There are " + errorCount + " error(s):\r\n\n";
                for (int i = 0; i < errorCount; i++) {
                    CompilerError currentError = errorReporter.get(i);
                    if (currentError.getClass() == LexicalError.class) {
                        text += "   Lexical error: " + currentError.Text() + "\r\n";
                    } else if (currentError.getClass() == SyntacticError.class) {
                        text += "   Syntactic error: " + currentError.Text() + "\r\n";
                    }
                }
            } else {
                text = "Easy Compiler (Java Version 2.0)\r\nSyntax Analysis...\r\n"
                        + "Compilation was successful.\n\n\n";
            }
            
            resultadoFinal=resultadoFinal+texto +"\n";
            erroresFinal=erroresFinal+text+"\n";
            
            }
            mainFrame.getScannerOutput().setText(resultadoFinal);
            mainFrame.getConsole().setText(erroresFinal);
        
    }

    private void addTokenToScannerOutput(Token current) {
        if (current.getPosition().getLine() > line) {
            line = current.getPosition().getLine();
            newLine();
        }
        String strToken = "<" + current.getKind().toString() + " , " + "\"" + current.getLexeme() + "\"" + ">";
        scannerOutput.add(strToken);
    }

    private String writeScannerOutput() {
        String text = "";
        String operaciones = "";
        int contador = 0;
        for (int i = 0; i < scannerOutput.size(); i++) {

//            if (scannerOutput.get(i) == "\r\n") {
//                text = text + " " + scannerOperacion.get(contador).getOperaResult();
//                contador = contador + 1;
//
//            }
            text = text + " " + scannerOutput.get(i);

            System.out.println(scannerOutput.get(i));

        }

//        for (int i = 0;  i<scannerOperacion.size(); i++) {
//           operaciones = operaciones + " " + scannerOperacion.get(i).getOperaResult();   
//          
//        }



       String operationes=" ";
       ArrayList<String>lis=scannerOperacion.get(contador).getListadoOperaciones();
       
        for (int i = lis.size()-1; i >=0 ; i--) {
            
        operationes=   operationes+ "  Operación#" + (i+1)+ " "+ lis.get(i) + "     "; 
            
        }
       
        text = text + " " + operationes;
        System.out.println(text);

      return text;
    }

    private void newLine() {
        scannerOutput.add("\r\n");
    }

    public void about() {
        new AboutDialog(mainFrame, true).setVisible(true);
    }

    public void grammar() throws MalformedURLException, IOException {
        GrammarDialog grammarDialog = new GrammarDialog(mainFrame, true);
        grammarDialog.getGrammarEditorPane().setEditable(false);
        java.net.URL url = grammarDialog.getClass().getResource("/Resources/gramar.html");
        if (url != null) {
            grammarDialog.getGrammarEditorPane().setPage(url);
        }
        grammarDialog.setSize(mainFrame.getWidth(), mainFrame.getHeight());
        grammarDialog.setVisible(true);
    }
}
