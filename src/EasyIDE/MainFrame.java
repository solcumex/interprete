/*
 * MainFrame.java
 *
 * Created on 11 de marzo de 2008, 23:15
 */

package EasyIDE;
/**
 *
 * @author DDC Programación, UCI
 */
import EasyCompiler.Connection.Conexion;
import EasyCompiler.Modelos.LevenshteinDistance;
import EasyCompiler.Modelos.Tarea;
import EasyCompiler.Semantic.*;
import static com.sun.org.apache.xalan.internal.lib.ExsltDynamic.map;
import java.awt.Dimension;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import static jdk.nashorn.internal.objects.NativeArray.map;
import static jdk.nashorn.internal.objects.NativeDebug.map;




public class MainFrame extends javax.swing.JFrame {
     private final Tareas_servicio tareas_servicio = new Tareas_servicio();
    private List<Tarea> tareas;
   List<String>corpustexto;
     Map<String,Integer> mapacomponengeneral= new HashMap<>();
     public int cantidadpalabrasconunaaparicion=0;
    
    /** Creates new form MainFrame */
    public MainFrame() {
        initComponents();
         this.cargar_lista_de_tareas();
         
         
            //File file = new File("C:\\inetpub\\corpus.txt");
         
           // guardarenfichero("Ander",file);
            
         
        cargarCorpus();
      
        
        Dimension d= new Dimension();
        System.out.println(jPanel1.getSize().width/2+"jpanel");
        System.out.println(jTable1.getSize().width +"jtable");
        jTable1.setSize(jPanel1.getSize().width/2,jPanel1.getSize().height);
        System.out.println(jTable1.getSize().width +"jtabledespues");
        
    }
    
     private void cargar_lista_de_tareas(){
        try{
            this.tareas = this.tareas_servicio.recuperarTodas(Conexion.obtener());
            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            dtm.setRowCount(0);
            for(int i = 0; i < this.tareas.size(); i++){
                dtm.addRow(new Object[]{
                    this.tareas.get(i).getTitulo(),
                    this.tareas.get(i).getDescripcion(),
                        this.tareas.get(i).getNivel_de_prioridad(),
                         this.tareas.get(i).getOperacion()
                });
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(this, "Ha surgido un error y no se han podido recuperar los registros");
        }catch(ClassNotFoundException ex){
            System.out.println(ex);
            JOptionPane.showMessageDialog(this, "Ha surgido un error y no se han podido recuperar los registros");
        }
    }
     
     
     
     
     public void NuevoCorpus( List<String> viejocorpus) throws SQLException, SQLException, ClassNotFoundException
     {
     ArrayList<String> nuevocorpus= new ArrayList<>();
              String auxe="";
              String oracion="";
             
     
         for (int i = 0; i < viejocorpus.size(); i++) {
             
             oracion="";
              auxe=viejocorpus.get(i);
              
              String [] palabras= auxe.split(" ");
              for (int j = 0; j < palabras.length; j++) {
                  
                  if (MainFrame.isNumeric(palabras[j])) {
                      
                      oracion=oracion +  "numero" + " ";
                  }
                  
                 
                  
                  if (("vector".equals(analisispalabra(palabras[j])))   || (analisispalabra(palabras[j]).equals("suma" ))||  (analisispalabra(palabras[j]).equals("resta")) || (analisispalabra(palabras[j]).equals("multiplicar"))|| ("dividir".equals(analisispalabra(palabras[j])))) {
                    oracion=oracion +  palabras[j] + " ";
                    continue;
                  
                  }
                  
//                  if (comprobarsilapalabrapertenecealasmasusadas(palabras[j])) {
//                      oracion=oracion +  palabras[j] + " ";
//                       continue;
//                  }
//                  
                  
                  if (comprobarpaalbrautilizandolivshtein(palabras[j])) {
                      
                        oracion=oracion +  palabras[j] + " ";
                         continue;
                      
                  }
                  else
                  {
                   oracion=oracion +  "BS" + " ";
                    continue;
                  
                  }
                  
                 
             }
              nuevocorpus.add(oracion);
              System.out.println(oracion);
              
              
              
         }
         
         
         
         
        List<String> sentenciasID = tareas_servicio.recuperarSetenciasid(Conexion.obtener());
        List<String> sentenciasTitulo = tareas_servicio.recuperarSetenciastitulo(Conexion.obtener());
        List<String> sentenciasdescripcion = tareas_servicio.recuperarSetencias(Conexion.obtener());
        List<String> sentenciasNIvelPrioridad = tareas_servicio.recuperarSetenciasnivel_de_prioridad(Conexion.obtener());
        List<String> sentenciasoperacion = tareas_servicio.recuperarSetenciasoperacion(Conexion.obtener());
        
        
         for (int i = 0; i < nuevocorpus.size(); i++) {
             
             tareas_servicio.guardarenTareasBa(Conexion.obtener(), Integer.parseInt(sentenciasID.get(i)), sentenciasTitulo.get(i),sentenciasdescripcion.get(i), Integer.parseInt(sentenciasNIvelPrioridad.get(i)),sentenciasoperacion.get(i) , nuevocorpus.get(i));
             
             
         }
     
     
         
         
         
         
         
         
         
         
         
         
         
     
//     return nuevocorpus;
     
     }
     
     
     
    
     
     public void cambiarBaseDatosPorbasura() throws SQLException, ClassNotFoundException
     {
         HashMap<Integer,String>mapa= new HashMap<>();
           HashMap<Integer,String>mapasuma= new HashMap<>();
             HashMap<Integer,String>maparesta= new HashMap<>();
             HashMap<Integer,String>mapamult= new HashMap<>();
               HashMap<Integer,String>mapadivisi= new HashMap<>();
                     HashMap<Integer,String>mapadvari= new HashMap<>();
                           HashMap<Integer,String>mapaNS= new HashMap<>();
                           
               
             
             
         
         String aux= "";
      List<String>corpus=this.tareas_servicio.recuperarSetencias(Conexion.obtener());
        List<String>corpusid=this.tareas_servicio.recuperarSetenciasid(Conexion.obtener());
      
      
      
         for (int i = 0; i < corpus.size(); i++) {
             aux=comprobarOperacion(corpus.get(i));
             
             mapa.put(i,aux );
             if (aux=="suma") {
                 mapasuma.put(i, corpus.get(i).toString());
                 tareas_servicio.actualizarOperacionesdetareas(Conexion.obtener(), Integer.parseInt(corpusid.get(i)) , aux);
             }
             if (aux=="resta") {
                     tareas_servicio.actualizarOperacionesdetareas(Conexion.obtener(), Integer.parseInt(corpusid.get(i)) , aux);
                 maparesta.put(i, corpus.get(i).toString());
                 
             }
             
             if (aux=="dividir") {
                 
                      tareas_servicio.actualizarOperacionesdetareas(Conexion.obtener(), Integer.parseInt(corpusid.get(i)) , aux);
                 mapadivisi.put(i, corpus.get(i).toString());
             }
             if (aux=="multiplicar") {
                     tareas_servicio.actualizarOperacionesdetareas(Conexion.obtener(), i+4, aux);
                 mapamult.put(i, corpus.get(i).toString());
                 
             }
             
             if (aux=="vector") {
                      tareas_servicio.actualizarOperacionesdetareas(Conexion.obtener(), Integer.parseInt(corpusid.get(i)) , aux);
                 mapadvari.put(i, corpus.get(i).toString());
                 
             }
             else{
                 
                     tareas_servicio.actualizarOperacionesdetareas(Conexion.obtener(), Integer.parseInt(corpusid.get(i)) , aux);
             mapaNS.put(i, corpus.get(i).toString());
             }
             System.out.println(" Mapa la posición: " + Integer.parseInt(corpusid.get(i)) +" es una " + aux + "   La Oracion es  " + corpus.get(i).toString());
             
         }
      
     
     }
     
     
         private static boolean isNumeric(String cadena){
    	try {
    		Integer.parseInt(cadena);
    		return true;
    	} catch (NumberFormatException nfe){
    		return false;
    	}
    }
         
         
         
     
     
     public String comprobarOperacion(String oracion)
     {
         String auxe= "";
     
         if ((oracion.contains("suma"))||(oracion.contains("utilizado")) ||(oracion.contains("utilizó"))||(oracion.contains("utiliza"))||(oracion.contains("compro"))||(oracion.contains("comprado "))||(oracion.contains("compra"))||(oracion.contains("total"))||(oracion.contains("Total"))||(oracion.contains("gastado"))||(oracion.contains("gasto "))||(oracion.contains("gastar "))||(oracion.contains("vendido"))||(oracion.contains("vender"))||(oracion.contains("gasto "))||(oracion.contains("más"))||(oracion.contains("sumar"))||(oracion.contains("sumo"))||(oracion.contains("Adición"))||(oracion.contains("adición"))||(oracion.contains("Adicion"))||(oracion.contains("adicion"))||(oracion.contains("juntar"))||(oracion.contains("junta"))||(oracion.contains("junto"))||(oracion.contains("congrego"))||(oracion.contains("Agregar"))||(oracion.contains("congregar"))||(oracion.contains("agregaron"))||(oracion.contains("agregar"))||(oracion.contains("agrega"))||(oracion.contains("agrego"))||(oracion.contains("poner"))||(oracion.contains("pongo"))||(oracion.contains("adiciono"))||(oracion.contains("adiciona"))||(oracion.contains("coloco"))||(oracion.contains("coloca"))||(oracion.contains("uno"))||(oracion.contains("colocamos"))||(oracion.contains("huno"))||(oracion.contains("Aditamento"))||(oracion.contains("aditamento"))||(oracion.contains("Complemento"))||(oracion.contains("complemento"))||(oracion.contains("Complementa"))||(oracion.contains("complementa"))||(oracion.contains("Añadidura"))||(oracion.contains("añadidura"))||(oracion.contains("añado"))||(oracion.contains("puso"))||(oracion.contains("puse"))||(oracion.contains("sumatoria"))||(oracion.contains(" di "))||(oracion.contains(" dar "))||(oracion.contains("compró"))||(oracion.contains("comprar"))||(oracion.contains(" unir "))||(oracion.contains(" unió "))||(oracion.contains(" dio "))||(oracion.contains("ganó"))||(oracion.contains("gana"))||(oracion.contains("une"))||(oracion.contains("ganar"))||(oracion.contains("gano"))||(oracion.contains("incremento"))||(oracion.contains("incrementó"))||(oracion.contains("incrementar"))||(oracion.contains("incrementó"))||(oracion.contains("adicionando"))||(oracion.contains("adicionada"))) 
         {
           auxe="suma";
           
         }
         else
         if  ((oracion.contains("resta"))||(oracion.contains("usado"))||(oracion.contains(" usa "))||(oracion.contains("ocupadas"))||(oracion.contains("menos"))||(oracion.contains("faltan"))||(oracion.contains("perdido"))||(oracion.contains("comido"))||(oracion.contains("lleva"))||(oracion.contains("resto"))||(oracion.contains("disminuir"))||(oracion.contains("quitar"))||(oracion.contains("romper"))||(oracion.contains("rompio"))||(oracion.contains("rompieron"))||(oracion.contains("sustraer"))||(oracion.contains("sustrajo"))||(oracion.contains("robo"))||(oracion.contains("Robo"))||(oracion.contains("Robó"))||(oracion.contains("rompio"))||(oracion.contains("rompieron"))||(oracion.contains("rompió"))||(oracion.contains("detraer"))||(oracion.contains("rebajar"))||(oracion.contains("rebajo"))||(oracion.contains("quito"))||(oracion.contains("aminoró"))||(oracion.contains("aminoro"))||(oracion.contains("redujo"))||(oracion.contains("sustrajo"))||(oracion.contains("sustraer"))||(oracion.contains("eliminar"))||(oracion.contains("eliminó"))||(oracion.contains("perder"))||(oracion.contains("perdio"))||(oracion.contains("perdimos"))||(oracion.contains("eliminamos"))||(oracion.contains("elimino"))||(oracion.contains("pierdo"))||(oracion.contains("perder"))) {
   
             auxe="resta";
                }
         
         
           else
         if  ((oracion.contains("segmentacion"))||(oracion.contains("Segmentación"))||(oracion.contains("Desmembramiento"))||(oracion.contains("desmembramiento"))||(oracion.contains("fragmentación"))||(oracion.contains("Fragmentación"))||(oracion.contains("Parte"))||(oracion.contains("parte"))||(oracion.contains("partición"))||(oracion.contains("fraccionar"))||(oracion.contains("fraccionamiento"))||(oracion.contains("subdivision"))||(oracion.contains("Subdivision"))||(oracion.contains("division"))||(oracion.contains("fragmento"))||(oracion.contains("Fragmento"))||(oracion.contains("Fragmentó"))||(oracion.contains("parte"))||(oracion.contains("rebajo"))||(oracion.contains("divide"))||(oracion.contains(" entre "))||(oracion.contains("dividió"))) {
   
             auxe="dividir";
                }
         
         
             else
         if  ((oracion.contains("multiplica"))||(oracion.contains("en cada" ))||(oracion.contains("multiplicación"))||(oracion.contains("multiplicado"))||(oracion.contains("multiplicando"))||(oracion.contains(" por "))||(oracion.contains("redoblar"))||(oracion.contains("elevar"))||(oracion.contains("eleva"))||(oracion.contains("propaga"))||(oracion.contains("propagar"))||(oracion.contains("reproducir"))||(oracion.contains("reproduce"))) {
   
             auxe="multiplicar";
                }
         
         
         
         
         else
//             ||(oracion.contains("sustrajo"))||(oracion.contains("robo"))||(oracion.contains("Robo"))||(oracion.contains("Robó"))||(oracion.contains("rompio"))||(oracion.contains("rompieron"))||(oracion.contains("rompió"))||(oracion.contains("detraer"))||(oracion.contains("rebajar"))||(oracion.contains("rebajo"))||(oracion.contains("quito"))||(oracion.contains("aminoró"))||(oracion.contains("aminoro"))||(oracion.contains("redujo"))||(oracion.contains("sustrajo"))||(oracion.contains("sustraer"))||(oracion.contains("eliminar"))||(oracion.contains("eliminó"))||(oracion.contains("perder"))||(oracion.contains("perdio"))||(oracion.contains("perdimos"))||(oracion.contains("eliminamos"))||(oracion.contains("elimino"))||(oracion.contains("pierdo"))||(oracion.contains("perder"))
         if  ((oracion.contains("vector"))||(oracion.contains("constante"))||(oracion.contains("variable"))||(oracion.contains("Variable"))||(oracion.contains("Constantes"))||(oracion.contains("atributo"))||(oracion.contains("Atributo"))||(oracion.contains("variables"))||(oracion.contains("vectores"))) {
   
             auxe="vector";
                }
         
         
         
         
         else{
         auxe="NS";
         }
         
       
 

 
         return auxe;
         
     }
     
     
     
    public void trimsetencias() throws SQLException, ClassNotFoundException
    {
    
          List<Tarea>corpusTareas=this.tareas_servicio.recuperarTodas(Conexion.obtener());
             
             int valorpalabra=0;
               String DescripString="";
             for (int i = 0; i < corpusTareas.size(); i++) {
                 
                  valorpalabra=corpusTareas.get(i).getId_tarea();
                
                 
                  DescripString =corpusTareas.get(i).getDescripcion();
                  DescripString = DescripString.replaceAll("\\r\\n|\\r|\\n", " ");
                  DescripString = DescripString.replaceAll("\\t", " ");
                  DescripString=DescripString.replaceAll(" +", " ");
                 DescripString=DescripString.trim();
                  System.out.println("ID:   "+   valorpalabra+  "    " +   DescripString);
                    System.out.println(corpusTareas.get(i).getDescripcion());
                 
                tareas_servicio.actualizarTareas(Conexion.obtener(), valorpalabra,DescripString );
                
            }
    }
    
    
    public boolean comprobarsilapalabrapertenecealasmasusadas(String palabra) throws SQLException, ClassNotFoundException
    
    {
        List<String>Palabras= tareas_servicio.obtenervalordepalabras(Conexion.obtener());
        
        
        boolean esta=false;
        if (Palabras.contains(palabra)) {
            
            esta=true;
          
            
        }
        else{
        esta=false;
        }
    return esta;
    
    }
    public List<String> listadodeoperaiconesdetectadas()
    {
    
    
     List<String>Palabras1=new ArrayList<>(Arrays.asList("suma", "mas", "gastar", "vendido", "vender", "utilizado", "utilizó" , "compro", "comprado", "total", "gastado", "Total", "gasto"               , "más", "sumar", "sumo", "Adición", "adición", "adicion" , "juntar", "junta", "junto", "congrego", "Agregar", "congregar"              , "agregaron", "agregar", "agrega", "agrego", "poner", "pongo" , "adiciono", "adiciona", "coloco", "coloca", "uno", "huno"               , "Aditamento", "aditamento", "Complemento", "complemento", "Complementa", "complementa" , "Añadidura", "añadidura", "añado", "puso", "puse", 
               "sumatoria",  "di", "dar", "compró", "comprar", "unir", "unió" , "dio", "ganó", "gana", "une", "ganar", "gano"               , "incremento", "incrementó", "incrementar", "adicionando", "adicionada",   "resta" , "Resta", "usado", "ocupadas", "menos", "faltan", "perdido"              , "comido", "lleva", "resto", "disminuir", "quitar", "romper" , "rompio", "rompieron", "sustraer", "sustrajo", "robo", "Robo"               , "Robó", "rompio", "rompieron", "rompió", "detraer", "rebajar" , "rebajo", "quito", "aminoró", "aminoro", "redujo",
               "sustrajo", "sustraer", "eliminar", "eliminó", "perder", "perdio" , "perdimos", "eliminamos", "elimino", "pierdo", "perder", "segmentacion"               , "Segmentación", "Desmembramiento", "desmembramiento", "fragmentación", "Fragmentación", "Parte" , "parte", "partición", "fraccionamiento", "subdivision", "Subdivision", "division"              , "fragmento", "Fragmento", "Fragmentó", "parte", "rebajo", "divide" , "entre", "dividió", "multiplica", "multiplicación", "multiplicado", "multiplicando"               , "por", "redoblar", "elevar", "eleva", "propaga", "propagar" , "reproducir", "reproduce",
                "vector", "constante", "variable", "Variable", "Constantes",   "atributo" , "Atributo", "variables", "vectores"  ));
     return Palabras1;
    }
    
    public void distanciadelivenshtein() throws SQLException, ClassNotFoundException
    {
        
        
 
       List<String>Palabras= tareas_servicio.obtenervalordepalabras(Conexion.obtener());
       List<String>Palabras1=new ArrayList<>(Arrays.asList("suma", "mas", "gastar", "vendido", "vender", "utilizado", "utilizó" , "compro", "comprado", "total", "gastado", "Total", "gasto"               , "más", "sumar", "sumo", "Adición", "adición", "adicion" , "juntar", "junta", "junto", "congrego", "Agregar", "congregar"              , "agregaron", "agregar", "agrega", "agrego", "poner", "pongo" , "adiciono", "adiciona", "coloco", "coloca", "uno", "huno"               , "Aditamento", "aditamento", "Complemento", "complemento", "Complementa", "complementa" , "Añadidura", "añadidura", "añado", "puso", "puse", 
               "sumatoria",  "di", "dar", "compró", "comprar", "unir", "unió" , "dio", "ganó", "gana", "une", "ganar", "gano"               , "incremento", "incrementó", "incrementar", "adicionando", "adicionada",   "resta" , "Resta", "usado", "ocupadas", "menos", "faltan", "perdido"              , "comido", "lleva", "resto", "disminuir", "quitar", "romper" , "rompio", "rompieron", "sustraer", "sustrajo", "robo", "Robo"               , "Robó", "rompio", "rompieron", "rompió", "detraer", "rebajar" , "rebajo", "quito", "aminoró", "aminoro", "redujo",
               "sustrajo", "sustraer", "eliminar", "eliminó", "perder", "perdio" , "perdimos", "eliminamos", "elimino", "pierdo", "perder", "segmentacion"               , "Segmentación", "Desmembramiento", "desmembramiento", "fragmentación", "Fragmentación", "Parte" , "parte", "partición", "fraccionamiento", "subdivision", "Subdivision", "division"              , "fragmento", "Fragmento", "Fragmentó", "parte", "rebajo", "divide" , "entre", "dividió", "multiplica", "multiplicación", "multiplicado", "multiplicando"               , "por", "redoblar", "elevar", "eleva", "propaga", "propagar" , "reproducir", "reproduce",
                "vector", "constante", "variable", "Variable", "Constantes",   "atributo" , "Atributo", "variables", "vectores"  ));
           HashMap<String,Integer>mapadistancia= new HashMap<>();
         List<HashMap>listadodistancia= new ArrayList<>();
         List<String> listadodecadena= new ArrayList<>();
         String palabr="";
         int valores=0;
         
         
         

         
         
         
         
            for (int i = 0; i < Palabras1.size(); i++) {
                
                for (int j = 0; j < Palabras.size(); j++) {
                    
                    
                    
                   
                    
                   valores=LevenshteinDistance.computeLevenshteinDistance(Palabras1.get(i), Palabras.get(j));
                   
                   mapadistancia.put(Palabras1.get(i)+"  ----"+Palabras.get(j), valores);
                   
                    System.out.println(Palabras1.get(i)+"  ----"+Palabras.get(j)+ "   la distancia es de:  "+ valores);
                    palabr=Palabras1.get(i).toString()+"  ----"+Palabras.get(j).toString()+ "   la distancia es de:  "+ valores;
                    listadodecadena.add(palabr);
                 
                    
                    
                    
                    
                    
                    
                }
                
                agregarTextoAlfinal("C:\\inetpub\\palabrasoperaciones.txt",listadodecadena);
                listadodecadena= new ArrayList<>();
                
                listadodistancia.add(mapadistancia);
                mapadistancia= new HashMap<>();
                
            }
            
            
    
    }
    
    
    public void agregarTextoAlfinal(String nombreArch,List<String>listado){
        int i;
        try {
            FileWriter fstream = new FileWriter(nombreArch, true);
            BufferedWriter out = new BufferedWriter(fstream);
            for(i=0;i<listado.size();i++){
                out.write(listado.get(i));
                out.newLine();
            }
            out.close();
        } catch (IOException ex) {
            System.out.println("Error: "+ex.getMessage());
        }
 }
    
    
    public void guardarenfichero(String palabr,File file)
    {
        
      
        
         FileWriter fichero = null;
        PrintWriter pw = null;
        BufferedWriter bw = null;
    
        try
        {
        
          for (int i = 0; i < 100; i++) {
            
         fichero = new FileWriter(file.getAbsoluteFile(), true);
			bw = new BufferedWriter(fichero);

			bw.write(i+"");

			System.out.println("Done");
//            pw = new PrintWriter(fichero);
//            pw.println(palabr);

//            for (int i = 0; i < 10; i++)
//                pw.println("Linea " + i);
 }
         
        } catch (Exception e) {
            e.printStackTrace();
         
        }finally {

			try {

				if (bw != null)
					bw.close();

				if (fichero != null)
					fichero.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}
        
        }
    }

    public int getCantidadpalabrasconunaaparicion() {
        return cantidadpalabrasconunaaparicion;
    }
    
    
    
    private void cargarCorpus()
    {
        
      
        Map<String,Integer> mapa= new HashMap<>();
        try {
            
         //this.tareas_servicio.reducirnumeracionBS();
       List<String>corpus=this.tareas_servicio.recuperarSetencias(Conexion.obtener());
       // tareas_servicio.recuperarTodasGuardarentablaHMM(Conexion.obtener());
            
  // List<String>corpus=this.tareas_servicio.recuperarSetenciasbasura(Conexion.obtener());
       //  List<String>corpus=this.tareas_servicio.recuperarSetenciasbasura(Conexion.obtener());
            
              //List<String>corpus=this.tareas_servicio.recuperarSetenciassuma(Conexion.obtener());
              //List<String>corpus=this.tareas_servicio.recuperarSetenciasResta(Conexion.obtener());
               // List<String>corpus=this.tareas_servicio.recuperarSetenciasMult(Conexion.obtener());
               //  List<String>corpus=this.tareas_servicio.recuperarSetenciasDivi(Conexion.obtener());
               
               
             corpustexto=corpus;
         bitrigramas bitrigrama=new bitrigramas(corpus);
        //NuevoCorpus(corpus);
        // distanciadelivenshtein();
         
         //trimsetencias();
                 
       
             
             
             
            
             
             
         //relacionado a la distancia de liveschtein
         
       
       //  List<String>Palabras= tareas_servicio.obtenerbigramas(Conexion.obtener());
//         String cadena1="Como";
//         String cadena2="como";
//         
         
         

         
         
         
         
//            for (int i = 0; i < Palabras.size(); i++) {
//                
//                for (int j = 0; j < Palabras.size(); j++) {
//                    
//                    
//                    
//                   
//                    
//                   valores=LevenshteinDistance.computeLevenshteinDistance(Palabras.get(i), Palabras.get(j));
//                   
//                   mapadistancia.put(Palabras.get(i)+"  ----"+Palabras.get(j), valores);
//                   
//                    System.out.println(Palabras.get(i)+"  ----"+Palabras.get(j)+ "   la distancia es de:  "+ valores);
//                    
//                    
//                    
//                    
//                }
//                
//                listadodistancia.add(mapadistancia);
//                mapadistancia= new HashMap<>();
//                
//            }
            
            
            
            
            
            
            
         
         
         
       // NuevoCorpus(corpus);
            
            //cambiarBaseDatosPorbasura();
         
            
            
              Map<String, Integer> bigramCounts = bitrigrama.getBigramCounts();
              Map<String, Integer> trigramCounts = bitrigrama.getTrigramCounts();
              Map<String, Integer> wordCounts = bitrigrama.getWordCounts();
              
              
               Iterator itecotar=wordCounts.keySet().iterator();
                while(itecotar.hasNext()){
            String key =  (String) itecotar.next();
           
            String aux= "Clave: " + key + " -> Valor: " + wordCounts.get(key);            
                    if (wordCounts.get(key)==1) {
                        cantidadpalabrasconunaaparicion=cantidadpalabrasconunaaparicion+1;
                    }
      
             
         
                              }
              
              
              int cantpalabras=bitrigrama.getCantpalabras();
            
        //tareas_servicio.GuardarWordcount(wordCounts);
             //tareas_servicio.trigramaBrigramas(Conexion.obtener(),bigramCounts);
            Iterator it = bigramCounts.keySet().iterator();
             DefaultListModel modelo = new DefaultListModel(); 
           while(it.hasNext()){
            String key =  (String) it.next();
           
            String aux= "Clave: " + key + " -> Valor: " + bigramCounts.get(key);            
            modelo.addElement(aux);         
             
         
                              }
           
            Iterator ite = trigramCounts.keySet().iterator();
           
             DefaultListModel modelo1 = new DefaultListModel(); 
           
           
           
               while(ite.hasNext()){
            String key = (String) ite.next();
           
            String aux= "Clave: " + key + " -> Valor: " + trigramCounts.get(key);            
            modelo1.addElement(aux);         
             
         
                              }
               
               
              jList1.setModel(modelo1);
              jList2.setModel(modelo);
              
              
              
              
              String auxe= jLabel1.getText();
              int valor= wordCounts.size();
              jLabel1.setText(auxe+String.valueOf(valor));
                  mapa.put("cantpalabrasdiferentes",valor) ;
                  jLabel5.setText(jLabel5.getText()+":" + bigramCounts.size());
                   jLabel6.setText(jLabel6.getText()+":" + trigramCounts.size());
              
             
              
              auxe= jLabel3.getText();
               valor= corpus.size();
              jLabel3.setText(auxe+String.valueOf(valor));
          
                mapa.put("cantoracionesdiferetes",valor) ;
              
              
               auxe= jLabel2.getText();
              
              jLabel2.setText(auxe+String.valueOf(cantpalabras));
              mapa.put("cantpalabra",cantpalabras) ;
              
              mapacomponengeneral =mapa;
              
              
          // tareas_servicio.guardaractualizarinformaciontareas(Conexion.obtener(), mapa);
              
             //tareas_servicio.guardaractualizarinformacion(Conexion.obtener(),mapa);
            
            
            
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(this, "Ha surgido un error y no se han podido recuperar los registros");
        }catch(ClassNotFoundException ex){
            System.out.println(ex);
            JOptionPane.showMessageDialog(this, "Ha surgido un error y no se han podido recuperar los registros");
        }
    
    
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar1 = new javax.swing.JToolBar();
        jSeparator3 = new javax.swing.JToolBar.Separator();
        newButton = new javax.swing.JButton();
        openButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JToolBar.Separator();
        compileButton = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane6 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jScrollPane7 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        sourceEditor = new javax.swing.JEditorPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        console = new javax.swing.JEditorPane();
        jScrollPane3 = new javax.swing.JScrollPane();
        scannerOutput = new javax.swing.JEditorPane();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        newMenuItem = new javax.swing.JMenuItem();
        openMenuItem = new javax.swing.JMenuItem();
        saveMenuItem = new javax.swing.JMenuItem();
        saveAsMenuItem = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        exitMenuItem = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        compileMenuItem = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        grammarMenuItem = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Easy IDE");
        setBounds(new java.awt.Rectangle(0, 0, 0, 0));

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);
        jToolBar1.add(jSeparator3);

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/new.jpg"))); // NOI18N
        newButton.setFocusable(false);
        newButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        newButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(newButton);

        openButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/open.jpg"))); // NOI18N
        openButton.setFocusable(false);
        openButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        openButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        openButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(openButton);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/save.JPG"))); // NOI18N
        saveButton.setFocusable(false);
        saveButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        saveButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(saveButton);
        jToolBar1.add(jSeparator2);

        compileButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/compile.jpg"))); // NOI18N
        compileButton.setFocusable(false);
        compileButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        compileButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        compileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                compileButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(compileButton);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Titulo", "Descripción", "Title 3"
            }
        ));
        jScrollPane5.setViewportView(jTable1);

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane6.setViewportView(jList1);

        jList2.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane7.setViewportView(jList2);

        jLabel1.setText("Palabras diferentes:");

        jLabel3.setText("Sentencias:");

        jLabel2.setText("Palabras totales:");

        jLabel5.setText("Bigramas");

        jLabel6.setText("Trigrama");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 909, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 281, Short.MAX_VALUE))
                    .addComponent(jScrollPane7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(315, 315, 315))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 523, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2))
                        .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING))
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane7)
                    .addComponent(jScrollPane6)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 567, Short.MAX_VALUE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Principal", jPanel1);

        sourceEditor.setFont(new java.awt.Font("Courier New", 0, 12)); // NOI18N
        jScrollPane1.setViewportView(sourceEditor);

        jTabbedPane1.addTab("Source", jScrollPane1);

        console.setFont(new java.awt.Font("Courier New", 0, 12)); // NOI18N
        jScrollPane2.setViewportView(console);

        jTabbedPane1.addTab("Console", jScrollPane2);

        scannerOutput.setFont(new java.awt.Font("Courier New", 0, 12)); // NOI18N
        scannerOutput.setName("scannerOutput"); // NOI18N
        jScrollPane3.setViewportView(scannerOutput);

        jTabbedPane1.addTab("Scanner Output", jScrollPane3);
        jTabbedPane1.addTab("Salida ", jTabbedPane2);

        jMenu1.setText("File");

        newMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/new.jpg"))); // NOI18N
        newMenuItem.setText("New");
        newMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(newMenuItem);

        openMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/open.jpg"))); // NOI18N
        openMenuItem.setText("Open");
        openMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(openMenuItem);

        saveMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/save.JPG"))); // NOI18N
        saveMenuItem.setText("Save");
        saveMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(saveMenuItem);

        saveAsMenuItem.setText("Save as...");
        saveAsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(saveAsMenuItem);
        jMenu1.add(jSeparator1);

        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(exitMenuItem);

        jMenuBar1.add(jMenu1);

        jMenu3.setText("Build");
        jMenu3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu3ActionPerformed(evt);
            }
        });

        compileMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/compile.jpg"))); // NOI18N
        compileMenuItem.setText("Build Program");
        compileMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                compileMenuItemActionPerformed(evt);
            }
        });
        jMenu3.add(compileMenuItem);

        jMenuItem2.setText("Matrizcuadrada");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem2);

        jMenuItem4.setLabel("TestMarkov");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem4);

        jMenuItem5.setText("Saber Oracion");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem5);

        jMenuItem1.setText("Markov");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem1);

        jMenuBar1.add(jMenu3);

        jMenu2.setText("Help");

        jMenuItem7.setText("About...");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem7);

        jMenuItem6.setText("Estadistica Ratio Corpus");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem6);

        grammarMenuItem.setText("Easy Grammar");
        grammarMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                grammarMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(grammarMenuItem);

        jMenuItem3.setLabel("Craw");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        jTabbedPane1.getAccessibleContext().setAccessibleName("tabControl");

        pack();
    }// </editor-fold>//GEN-END:initComponents


    
    public  double buscarmediadepalaabras()
    {   
        double valor=0.0;
        
        
        ArrayList<String> listado=(ArrayList) bitrigramas.getSingletonInstance().getPalabrastodas();
         ArrayList<Integer> listadoaux= new ArrayList<>();
         int sumar=0;
        
        for (int i = 0; i < listado.size(); i++) {
            
            
            
            listadoaux.add(listado.get(i).length());
            
        }
        
        for (int i = 0; i < listadoaux.size(); i++) {
            sumar = sumar+listadoaux.get(i);
            
        }
        
        valor= sumar/listadoaux.size();
        
        
        return valor;
    }
    
    
    
    
    
    
    
    
      public  Map<String,Integer> buscarpalabraconnumer()
    {   
        double valor=0.0;
        Map<String,Integer>mapage= new HashMap<>();
        
        ArrayList<String> listado=(ArrayList) bitrigramas.getSingletonInstance().getPalabrastodas();
         ArrayList<Integer> listadoaux= new ArrayList<>();
         int sumar=0;
          int sumar2=0;
           int sumar3=0;
            int sumar4=0;
            int sumar5=0;
           int sumar6=0;
            int sumar7=0;
            int sumar8=0;
           int sumar9=0;
            int sumar10=0;
        
        for (int i = 0; i < listado.size(); i++) {
            
            
            if (listado.get(i).length()==1) {
               // listadoaux.add(listado.get(i).length());
               sumar = sumar  +1;
            }
            
            
            
             if (listado.get(i).length()==2) {
               // listadoaux.add(listado.get(i).length());
               sumar2 = sumar2  +1;
            }
           
             
               if (listado.get(i).length()==3) {
               // listadoaux.add(listado.get(i).length());
               sumar3 = sumar3  +1;
            }
            
             if (listado.get(i).length()==4) {
               // listadoaux.add(listado.get(i).length());
               sumar4 = sumar4  +1;
            }
             
             
             
             
             if (listado.get(i).length()==5) {
               // listadoaux.add(listado.get(i).length());
               sumar5 = sumar5  +1;
            }
            
             if (listado.get(i).length()==6) {
               // listadoaux.add(listado.get(i).length());
               sumar6 = sumar6  +1;
            }
           
             
               if (listado.get(i).length()==7) {
               // listadoaux.add(listado.get(i).length());
               sumar7 = sumar7  +1;
            }
            
             if (listado.get(i).length()==8) {
               // listadoaux.add(listado.get(i).length());
               sumar8 = sumar8  +1;
            }
             
             
               if (listado.get(i).length()==9) {
               // listadoaux.add(listado.get(i).length());
               sumar9 = sumar9  +1;
            }
            
             if (listado.get(i).length()==10) {
               // listadoaux.add(listado.get(i).length());
               sumar10 = sumar10  +1;
            }
             
             
            
             
             
            
        }
        
        
         mapage.put("sumar", sumar);
         mapage.put("sumar2", sumar2);
          mapage.put("sumar3", sumar3);
          mapage.put("sumar4", sumar4);
          mapage.put("sumar5", sumar5);
           mapage.put("sumar6", sumar6); 
           mapage.put("sumar7", sumar7);
             mapage.put("sumar8", sumar8); 
           mapage.put("sumar9", sumar9);
                mapage.put("sumar10", sumar10);
           
           
           
          
          
//        for (int i = 0; i < listadoaux.size(); i++) {
//            sumar = sumar+listadoaux.get(i);
//            
//        }
//        
//        valor= sumar/listadoaux.size();
        
        
        return mapage;
    }
      
      
      
      
    private void newMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newMenuItemActionPerformed
        // TODO add your handling code here:
        try
        {
            IDEManager.getInstance().newDocument();
        }
        catch (Exception e)
        {
        
        }
    }//GEN-LAST:event_newMenuItemActionPerformed

    private void openMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openMenuItemActionPerformed
        // TODO add your handling code here:
        try
        {
            IDEManager.getInstance().open();
        }
        catch (Exception e)
        {
        
        }
    }//GEN-LAST:event_openMenuItemActionPerformed

    private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveMenuItemActionPerformed
        // TODO add your handling code here:
        try
        {
            IDEManager.getInstance().save();
        }
        catch(Exception e)
        {
        
        }
    }//GEN-LAST:event_saveMenuItemActionPerformed

    private void saveAsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAsMenuItemActionPerformed
        // TODO add your handling code here:
        try
        {
            IDEManager.getInstance().saveAs();
        }
        catch(Exception e)
        {
        
        }
    }//GEN-LAST:event_saveAsMenuItemActionPerformed

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        // TODO add your handling code here:
        try
        {
            IDEManager.getInstance().exit();
        }
        catch(Exception e)
        {
        
        }
    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void jMenu3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu3ActionPerformed
        // TODO add your handling code here:
        try
        {
            IDEManager.getInstance().build();
        }
        catch(Exception e)
        {
        
        }
    }//GEN-LAST:event_jMenu3ActionPerformed

    private void compileMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_compileMenuItemActionPerformed
        // TODO add your handling code here:
        try
        {
            IDEManager.getInstance().build();
        }
        catch(Exception e)
        {
        
        }
}//GEN-LAST:event_compileMenuItemActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        // TODO add your handling code here:
        IDEManager.getInstance().about();
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void grammarMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_grammarMenuItemActionPerformed
        try {
            // TODO add your handling code here:
            IDEManager.getInstance().grammar();
        } catch (MalformedURLException ex) {//GEN-LAST:event_grammarMenuItemActionPerformed
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        // TODO add your handling code here:
        try
        {
            IDEManager.getInstance().newDocument();
        }
        catch (Exception e)
        {
        
        }
    }//GEN-LAST:event_newButtonActionPerformed

    private void openButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openButtonActionPerformed
        // TODO add your handling code here:
        try
        {
            IDEManager.getInstance().open();
        }
        catch (Exception e)
        {
        
        }
    }//GEN-LAST:event_openButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        // TODO add your handling code here:
        try
        {
            IDEManager.getInstance().save();
        }
        catch(Exception e)
        {
        
        }
    }//GEN-LAST:event_saveButtonActionPerformed

    private void compileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_compileButtonActionPerformed
        // TODO add your handling code here:
        try
        {
            IDEManager.getInstance().build();
        }
        catch(Exception e)
        {
        
        }
    }//GEN-LAST:event_compileButtonActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed

        
          bitrigramas bitrigrama= new bitrigramas(corpustexto);
        MainFrame.this.dispose();
        Markov vista = new Markov(bitrigrama.getWordCounts());
        vista.setVisible(true);
        vista.setLocationRelativeTo(null);

        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    
    public ArrayList<String> cuadrarmatrizcon0(ArrayList<String>vergo,int cantmayor,int cant)
    {
    ArrayList<String>listado= new ArrayList<>();
             String auxformarpalabra="";
              String auxformarpalabracon0="";
            
    
                  for (int i = 0; i < vergo.size(); i++) {
                 
                 
                 String aux[]= vergo.get(i).split(" ");
                 int value= aux.length;
                 
                      for (int j = 0; j < cant; j++) {
                          
                          if (j<value) {
                             
                              auxformarpalabra=auxformarpalabra + aux[j]+",";
                              
                              
                          }
                          else
                          {
                              
                          auxformarpalabracon0=auxformarpalabracon0+"0,";
                          }
                          
                          
                          
                          
                          
                          
                      }
                      
                      auxformarpalabra=auxformarpalabra+auxformarpalabracon0;
                      
                      listado.add(auxformarpalabra);
                      auxformarpalabracon0="";
                      auxformarpalabra="";
                 
                 
             
                  }
                  
                  
                  return listado;
    }
    
    
        public ArrayList<String> cuadrarmatrizconrelleno(ArrayList<String>vergo,int cantmayor,int cant)
    {
    ArrayList<String>listado= new ArrayList<>();
      ArrayList<String>almacentemporal= new ArrayList<>();
             String auxformarpalabra="";
              String auxformarpalabracon0="";
              int puntero=0;
            
    
                  for (int i = 0; i < vergo.size(); i++) {
                 
                 
                 String aux[]= vergo.get(i).split(" ");
                 
                      for (int j = 0; j < aux.length; j++) {
                          
                          if (!"42".equals(aux[j])) {
                              
                              almacentemporal.add(aux[j]);
                          }
                      }
                 
                 int value= aux.length;
                 
                      for (int j = 0; j < cant; j++) {
                          
                          
                          if (j<value) {
                             
                              auxformarpalabra=auxformarpalabra + aux[j]+" ";
                              
                              
                          }
                          else
                          {
                               auxformarpalabracon0=auxformarpalabracon0+almacentemporal.get(puntero)+" ";
                              
                              if (puntero>=almacentemporal.size()-1) {
                                 
                                     puntero=0;
                              }else
                              {
                           
                              
                               puntero=puntero+1;
                              }
                         
                           //auxformarpalabracon0=auxformarpalabracon0+"0 ";
                          }
                          
                          
                          
                          
                          
                          
                      }
                      
                      auxformarpalabra=auxformarpalabra+auxformarpalabracon0;
                      
                      listado.add(auxformarpalabra);
                      auxformarpalabracon0="";
                      auxformarpalabra="";
                 
                 
             
                  }
                  
                  
                  return listado;
    }
    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
         try {
             // TODO add your handling code here:
             
             Tareas_servicio tarea = new Tareas_servicio();
             ArrayList<String> vergo= (ArrayList<String>) tarea.recuperarSetenciasparacuadrar(Conexion.obtener());
             for (int i = 0; i < vergo.size(); i++) {
                 
               //  System.out.println(vergo.get(i));
                 
             }
             
             
             
             
             
             ArrayList<String>listado= new ArrayList<>();
               ArrayList<String>listadoconpuntocoma= new ArrayList<>();
             String auxformarpalabra="";
              String auxformarpalabracon0="";
             int cantmayor=0;
             int cant=0;
             
             
             
             for (int i = 0; i < vergo.size(); i++) {
                 
                 
                 String aux[]= vergo.get(i).split(" ");
                 
                 
               
                     
                      if (aux.length>cant) {
                          System.out.println(i+"-- "+aux.length);
                     cant=aux.length;
                     cantmayor=i;
                     
                
                     
                 }
                      
                 
                
               
                 
             }
             
             
    listado=    cuadrarmatrizcon0(vergo,cantmayor,cant);
  //listado=    cuadrarmatrizconrelleno(vergo,cantmayor,cant);
             
            
             
             
             
             String auxiliate="";
             
                  
                  for (int i = 0; i < listado.size(); i++) {
                      
                      auxiliate=listado.get(i)+";";
                      listadoconpuntocoma.add(auxiliate);
                              
                      
                      //System.out.println(listado.get(i));
                      auxiliate="";
                 
             }
                    
                  for (int i = 0; i < listado.size(); i++) {
                      
                    //  System.out.println(listado.get(i).split(",").length);
                      System.out.println(listadoconpuntocoma.get(i));
                 
             }
             
             
             
             
             
             
         } catch (SQLException ex) {
             Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
         } catch (ClassNotFoundException ex) {
             Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
         }
        
        
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    
    
    
public Map<String,String> getContenidoHTML(String urle) throws IOException {
    URL url = new URL(urle);
    URLConnection uc = url.openConnection();
    uc.connect();
    //Creamos el objeto con el que vamos a leer
    BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
    String inputLine;
    String contenido = "";
    while ((inputLine = in.readLine()) != null) {
        contenido += inputLine + "<br\\>";
    }
    in.close();
    System.out.println(contenido);
    return paresearofertas(contenido);
     
}

public Map<String,String> paresearofertas(String valor)
{
    Map<String,String> mapa= new HashMap<>();
    String campovalor=valor;
    String auxiliar="";
    if (campovalor.contains("Detalle de la oferta ")) {
      String [] detalledeoferta=  campovalor.split("Detalle de la oferta ");
      String detalleofer=detalledeoferta[3];
      String detall=detalleofer.substring(detalleofer.indexOf("<small>")+"<small>".length(),detalleofer.indexOf("</small>"));
        System.out.println(detall);
        
        mapa.put("Detalle de la Oferta", detall);
            
    }
    campovalor=valor;
    
      if (campovalor.contains("Salario neto mensual:")) {
      String [] detalledeoferta=  campovalor.split("Salario neto mensual:");
      String detalleofer=detalledeoferta[1];
      String detall=detalleofer.substring(detalleofer.indexOf("<span>")+"<span>".length(),detalleofer.indexOf("</span>"));
        System.out.println(detall);
        
        mapa.put("Salario neto mensual:", detall);
            
    }
    
    campovalor=valor;
    
       if (campovalor.contains("Ubicación:")) {
      String [] detalledeoferta=  campovalor.split("Ubicación:");
      String detalleofer=detalledeoferta[1];
      String detall=detalleofer.substring(detalleofer.indexOf("<span>")+"<span>".length(),detalleofer.indexOf("</span>"));
        System.out.println(detall);
        
        mapa.put("Ubicación:", detall);
            
    }
    
       campovalor=valor;
       
       
              if (campovalor.contains("Tipo de contrato:")) {
      String [] detalledeoferta=  campovalor.split("Tipo de contrato:");
      String detalleofer=detalledeoferta[1];
      String detall=detalleofer.substring(detalleofer.indexOf("<span>")+"<span>".length(),detalleofer.indexOf("</span>"));
        System.out.println(detall);
        
        mapa.put("Tipo de contrato:", detall);
            
    }
    
       campovalor=valor;
       
       
       
                     if (campovalor.contains("Vigencia de la oferta:")) {
      String [] detalledeoferta=  campovalor.split("Vigencia de la oferta:");
      String detalleofer=detalledeoferta[1];
      String detall=detalleofer.substring(detalleofer.indexOf("<span>")+"<span>".length(),detalleofer.indexOf("</span>"));
        System.out.println(detall);
        
        mapa.put("Vigencia de la oferta:", detall);
            
    }
    
       campovalor=valor;
       
       
       
                    if (campovalor.contains("Días laborales:")) {
      String [] detalledeoferta=  campovalor.split("Días laborales:");
      String detalleofer=detalledeoferta[1];
      String detall=detalleofer.substring(detalleofer.indexOf("<span>")+"<span>".length(),detalleofer.indexOf("</span>"));
        System.out.println(detall);
        
        mapa.put("Días laborales:", detall);
            
    }
    
       campovalor=valor;
       
       
                         if (campovalor.contains("Horario de trabajo:")) {
      String [] detalledeoferta=  campovalor.split("Horario de trabajo:");
      String detalleofer=detalledeoferta[1];
      String detall=detalleofer.substring(detalleofer.indexOf("<span>")+"<span>".length(),detalleofer.indexOf("</span>"));
        System.out.println(detall);
        
        mapa.put("Horario de trabajo:", detall);
            
    }
    
       campovalor=valor;
       
       
                            if (campovalor.contains("Rolar turnos:")) {
      String [] detalledeoferta=  campovalor.split("Rolar turnos:");
      String detalleofer=detalledeoferta[1];
      String detall=detalleofer.substring(detalleofer.indexOf("<span>")+"<span>".length(),detalleofer.indexOf("</span>"));
        System.out.println(detall);
        
        mapa.put("Rolar turnos:", detall);
            
    }
    
       campovalor=valor;
       
       
       
       
       
       
       
                             if (campovalor.contains("Estudios Solicitados:")) {
      String [] detalledeoferta=  campovalor.split("Estudios Solicitados:");
      String detalleofer=detalledeoferta[1];
      String detall=detalleofer.substring(detalleofer.indexOf("<div class=\"especificos\">")+"<div class=\"especificos\">".length(),detalleofer.indexOf("</div>"));
        System.out.println(detall);
        
        mapa.put("Estudios Solicitados:", detall);
            
    }
    
       campovalor=valor;
       
       
                                   if (campovalor.contains("Competencias transversales:")) {
      String [] detalledeoferta=  campovalor.split("Competencias transversales:");
      String detalleofer=detalledeoferta[1];
      String detall=detalleofer.substring(detalleofer.indexOf("</strong>")+"</strong>".length(),detalleofer.indexOf("</div>"));
        System.out.println(detall);
        
        mapa.put("Competencias transversales:", detall);
            
    }
    
       campovalor=valor;
       
       
                            if (campovalor.contains("Idiomas:")) {
      String [] detalledeoferta=  campovalor.split("Idiomas:");
      String detalleofer=detalledeoferta[1];
      String detall=detalleofer.substring(detalleofer.indexOf("<div class=\"especificos\">")+"<div class=\"especificos\">".length(),detalleofer.indexOf("</div>"));
        System.out.println(detall);
        
        mapa.put("Idiomas:", detall);
            
    }
    
       campovalor=valor;
       
       
       
                     if (campovalor.contains("Prestaciones:")) {
      String [] detalledeoferta=  campovalor.split("Prestaciones:");
      String detalleofer=detalledeoferta[1];
      String detall=detalleofer.substring(detalleofer.indexOf("<div class=\"especificos\">")+"<div class=\"especificos\">".length(),detalleofer.indexOf("</div>"));
        System.out.println(detall);
        
        mapa.put("Prestaciones:", detall);
            
    }
    
       campovalor=valor;
       
       
       
       
       if (campovalor.contains("Número de plazas:")) {
      String [] detalledeoferta=  campovalor.split("Número de plazas:");
      String detalleofer=detalledeoferta[1];
      String detall=detalleofer.substring(detalleofer.indexOf("<div class=\"especificos\">")+"<div class=\"especificos\">".length(),detalleofer.indexOf("</div>"));
        System.out.println(detall);
        
        mapa.put("Número de plazas:", detall);
            
    }
    
       campovalor=valor;
       
       
       
              if (campovalor.contains("Funciones y actividades a realizar")) {
      String [] detalledeoferta=  campovalor.split("Funciones y actividades a realizar");
      String detalleofer=detalledeoferta[1];
      int auxees=detalleofer.indexOf("panel panelOfferOdd")+"panel panelOfferOdd".length();
      int auxes2=detalleofer.indexOf("<div class=\"col-sm-12\">");
      
      String detall=detalleofer.substring(auxees,auxes2);
      
      detall=detall.substring(detall.indexOf("<div class=\"panel-body\">")+"<div class=\"panel-body\">".length(),detall.indexOf("</div>"));
        System.out.println(detall);
        
        mapa.put("Funciones y actividades a realizar", detall);
            
    }
    
       campovalor=valor;
       
       
                 if (campovalor.contains("Observaciones")) {
      String [] detalledeoferta=  campovalor.split("Observaciones");
      String detalleofer=detalledeoferta[1];
      String detall=detalleofer.substring(detalleofer.indexOf("panel panelOfferOdd")+"panel panelOfferOdd".length(),detalleofer.indexOf("<div class=\"col-sm-12\">"));
       
      
       detall=detall.substring(detall.indexOf("<div class=\"panel-body\">")+"<div class=\"panel-body\">".length(),detall.indexOf("</div>"));
      
      
      
      System.out.println(detall);
        
        mapa.put("Observaciones", detall);
            
    }
    
       campovalor=valor;
       
       
       
    return mapa;
    
    
    

}


//public void llenarJson( Map<String,String>mapa)
//{
//    
//    
//               JSONObject obj = new JSONObject();
//         try {
//    
//   
//   for ( Map.Entry<String, String> entry : mapa.entrySet() )
//    {
//         obj.put(entry.getKey(), entry.getValue());
//    System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
//    }
//
//
//
////             obj.put("Blog", "http://javainutil.blogspot.com");
////        
////		obj.put("Temas", "Informatica");
////		obj.put("Inicio", new Integer(2012));
////		
////		JSONArray list = new JSONArray();
////                
////		list.put("tag 1");
////		list.put("tag 2");
////		list.put("tag 3");
////
////		obj.put("Tags", list);
////		
////		JSONObject innerObj = new JSONObject();
////		innerObj.put("PostX","Escribir un JSON");
////		innerObj.put("PostY", "Leer un JSON");
////		innerObj.put("PostZ", "lalala");
////		
////		obj.put("Posts",innerObj);
//                 } catch (JSONException ex) {
//             Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
//         }
//
//		try {
//                    
//
//			FileWriter file = new FileWriter("c:\\inetpub\\prueba.json");
//			file.write(obj.toString());
//			file.flush();
//			file.close();
//
//		} catch (IOException e) {
//			//manejar error
//		}
//
//		System.out.print(obj);
//
//
//}


    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
         try {
             Map<String,Map<String,String>> mapeador= new HashMap<>();
             // TODO add your handling code here:
             
             String nombre="";
             
            
             for (int i = 800; i < 999; i++) {
                 if (i<10) {
                     nombre="00"+i;
                 }if (i>10 && i<100) {
                     
                     nombre="0"+i;
                 }else 
                 {
                     nombre= String.valueOf(i);
                             }
                        
                 
             
             
             String nombr2="https://www.empleo.gob.mx/detalleoferta.do?method=init&id_oferta_empleo=3869"+nombre;
             //nombre = JOptionPane.showInputDialog("¿Cual es la URL?");
             Map<String,String> contenido=   getContenidoHTML(nombr2);
             mapeador.put(nombr2, contenido);
             
             
                 System.out.println("*********************************" + i);
             
             
             }
             
             try {
                 tareas_servicio.guardarmapapublicaciones(mapeador);
                 
                 // System.out.println(nombre);
             } catch (ClassNotFoundException ex) {
                 Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
             } catch (SQLException ex) {
                 Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
             }
             
             
             
         } catch (IOException ex) {
             Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
         }
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed

        generarAleatorio();
        TestMarkov test= new TestMarkov();
        test.setVisible(rootPaneCheckingEnabled);

        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
      
        String nombre;
  nombre = JOptionPane.showInputDialog("¿Cual es tu nombre?");
  String cadenacompleta="";
  String local="";
  
  
  String [] arreglo = nombre.split(" ");
        if (arreglo.length>1) {
            
            for (int i = 0; i < arreglo.length; i++) {
                
                
                try {
                    
                    if (!"0".equals(arreglo[i])) {                     
                                      
                    local=local+tareas_servicio.BuscarInformacion(arreglo[i])+" ";
                     }
                } catch (SQLException ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                
            }
            
        }
        else{
            
            
         arreglo = nombre.split(",");
        
         if (arreglo.length>1) {
             
                    for (int i = 0; i < arreglo.length; i++) {
                
                
                try {
                     if (!"0".equals(arreglo[i])) {  
                    local=local+tareas_servicio.BuscarInformacion(arreglo[i])+" ";
                     }
                } catch (SQLException ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                
            }
            
        }
        
        }
        
     
        
         JOptionPane.showMessageDialog(null,  local,"Mensaje", JOptionPane.INFORMATION_MESSAGE);
  
  System.out.println("Hola "+ local);

// TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        // TODO add your handling code here:
       Map<String,Integer>auxiliate= mapacomponengeneral;
       
       String cantpalabrasdiferentes=auxiliate.get("cantpalabrasdiferentes").toString();
         String cantoracionesdiferetes=auxiliate.get("cantoracionesdiferetes").toString();
         String cantpalabra=auxiliate.get("cantpalabra").toString();  
         String Ratio =String.valueOf( Double.parseDouble(cantpalabrasdiferentes) /Double.parseDouble(cantpalabra));
         String StandartRatio= String.valueOf( Double.parseDouble(cantpalabrasdiferentes)*100 /Double.parseDouble(cantpalabra)   );
         
         
       
        System.out.println("Cantidad de palabras "+ cantpalabra);
            System.out.println("Cantidad de palabras diferentes "+  cantpalabrasdiferentes);
            System.out.println("Cantidad de sentencias  "+cantoracionesdiferetes);
                      System.out.println("Ratio  " + Ratio);
                        System.out.println("Standart RAtio  " + StandartRatio);
        System.out.println("Media del tama;o de la palbra  " + buscarmediadepalaabras());
        
        
         Map<String,Integer> auxiliata=buscarpalabraconnumer();
         
        
         System.out.println("Tamaño de 1 letra  " + auxiliata.get("sumar"));
          System.out.println("Tamaño de 2 letra  " + auxiliata.get("sumar2"));
           System.out.println("Tamaño de 3 letra  " + auxiliata.get("sumar3"));
          System.out.println("Tamaño de 4 letra  " + auxiliata.get("sumar4"));
            System.out.println("Tamaño de 5 letra  " + auxiliata.get("sumar5"));
          System.out.println("Tamaño de 6 letra  " + auxiliata.get("sumar6"));
           System.out.println("Tamaño de 7 letra  " + auxiliata.get("sumar7"));
          System.out.println("Tamaño de 8 letra  " + auxiliata.get("sumar8"));
               System.out.println("Tamaño de 9 letra  " + auxiliata.get("sumar9"));
          System.out.println("Tamaño de 10 letra  " + auxiliata.get("sumar10"));
          
           double transeldivision=(Double.parseDouble( String.valueOf(getCantidadpalabrasconunaaparicion())) /  Double.parseDouble(cantpalabrasdiferentes) );
          double transel= (100* Math.log(Double.parseDouble(cantpalabra) ))/(1-transeldivision);
         
          System.out.println("Indice de Riqueza Lexica  " + transel);
            
  //      Map<String,Integer>
        
       
      
                   
              
        
        
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run(){
                try {
                    UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
                    IDEManager.getInstance().getMainFrame().setVisible(true);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                } catch (UnsupportedLookAndFeelException ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton compileButton;
    private javax.swing.JMenuItem compileMenuItem;
    private javax.swing.JEditorPane console;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenuItem grammarMenuItem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JList<String> jList1;
    private javax.swing.JList<String> jList2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JToolBar.Separator jSeparator2;
    private javax.swing.JToolBar.Separator jSeparator3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JButton newButton;
    private javax.swing.JMenuItem newMenuItem;
    private javax.swing.JButton openButton;
    private javax.swing.JMenuItem openMenuItem;
    private javax.swing.JMenuItem saveAsMenuItem;
    private javax.swing.JButton saveButton;
    private javax.swing.JMenuItem saveMenuItem;
    private javax.swing.JEditorPane scannerOutput;
    private javax.swing.JEditorPane sourceEditor;
    // End of variables declaration//GEN-END:variables

    public javax.swing.JEditorPane getSourceEditor() {
        sourceEditor.getDocument().addDocumentListener(new DocumentListener() {
            public void insertUpdate(DocumentEvent e) {
                IDEManager.getInstance().sourceEditorTextChanged();
            }

            public void removeUpdate(DocumentEvent e) {
                IDEManager.getInstance().sourceEditorTextChanged();
            }

            public void changedUpdate(DocumentEvent e) {
                IDEManager.getInstance().sourceEditorTextChanged();
            }
        });
        return sourceEditor;
    }

    public javax.swing.JTabbedPane getJTabbedPane1() {
        return jTabbedPane1;
    }
    
    public javax.swing.JEditorPane getScannerOutput()
    {
        return scannerOutput;
    }
    
    public javax.swing.JEditorPane getConsole() 
    {
        return console;
    }
    
    public javax.swing.JScrollPane getJScrollPane1()
    {
        return jScrollPane1;
    }
    
    public javax.swing.JScrollPane getJScrollPane2()
    {
        return jScrollPane2;
    }    
     
    public javax.swing.JScrollPane getJScrollPane3()
    {
        return jScrollPane3;
    }

    private String analisispalabra(String oracion) {
       
       String auxe="";
       if ((oracion.equals("suma"))||(oracion.equals("mas"))||(oracion.equals("gastar"))||(oracion.equals("vendido"))||(oracion.equals("vender"))||(oracion.equals("utilizado"))||(oracion.equals("utilizó"))||(oracion.equals("compro"))||(oracion.equals("comprado"))||(oracion.equals("total"))||(oracion.equals("gastado"))||(oracion.equals("Total"))||(oracion.equals("gasto"))||(oracion.equals("más"))||(oracion.equals("sumar"))||(oracion.equals("sumo"))||(oracion.equals("Adición"))||(oracion.equals("adición"))||(oracion.equals("Adicion"))||(oracion.equals("adicion"))||(oracion.equals("juntar"))||(oracion.equals("junta"))||(oracion.equals("junto"))||(oracion.equals("congrego"))||(oracion.equals("Agregar"))||(oracion.equals("congregar"))||(oracion.equals("agregaron"))||(oracion.equals("agregar"))||(oracion.equals("agrega"))||(oracion.equals("agrego"))||(oracion.equals("poner"))||(oracion.equals("pongo"))||(oracion.equals("adiciono"))||(oracion.equals("adiciona"))||(oracion.equals("coloco"))||(oracion.equals("coloca"))||(oracion.equals("uno"))||(oracion.equals("colocamos"))||(oracion.equals("huno"))||(oracion.equals("Aditamento"))||(oracion.equals("aditamento"))||(oracion.equals("Complemento"))||(oracion.equals("complemento"))||(oracion.equals("Complementa"))||(oracion.equals("complementa"))||(oracion.equals("Añadidura"))||(oracion.equals("añadidura"))||(oracion.equals("añado"))||(oracion.equals("puso"))||(oracion.equals("puse"))||(oracion.equals("sumatoria"))||(oracion.equals(" di "))||(oracion.equals(" dar "))||(oracion.equals("compró"))||(oracion.equals("comprar"))||(oracion.equals(" unir "))||(oracion.equals(" unió "))||(oracion.equals(" dio "))||(oracion.equals("ganó"))||(oracion.equals("gana"))||(oracion.equals("une"))||(oracion.equals("ganar"))||(oracion.equals("gano"))||(oracion.equals("incremento"))||(oracion.equals("incrementó"))||(oracion.equals("incrementar"))||(oracion.equals("incrementó"))||(oracion.equals("adicionando"))||(oracion.equals("adicionada"))) 
         {
             
           auxe="suma";
           return auxe;
           
         }
         else
         if  ((oracion.equals("resta"))||(oracion.equals("Resta"))||(oracion.equals("usado"))||(oracion.equals("ocupadas"))||(oracion.equals("menos"))||(oracion.equals("faltan"))||(oracion.equals("perdido"))||(oracion.equals("comido"))||(oracion.equals("lleva"))||(oracion.equals("resto"))||(oracion.equals("disminuir"))||(oracion.equals("quitar"))||(oracion.equals("romper"))||(oracion.equals("rompio"))||(oracion.equals("rompieron"))||(oracion.equals("sustraer"))||(oracion.equals("sustrajo"))||(oracion.equals("robo"))||(oracion.equals("Robo"))||(oracion.equals("Robó"))||(oracion.equals("rompio"))||(oracion.equals("rompieron"))||(oracion.equals("rompió"))||(oracion.equals("detraer"))||(oracion.equals("rebajar"))||(oracion.equals("rebajo"))||(oracion.equals("quito"))||(oracion.equals("aminoró"))||(oracion.equals("aminoro"))||(oracion.equals("redujo"))||(oracion.equals("sustrajo"))||(oracion.equals("sustraer"))||(oracion.equals("eliminar"))||(oracion.equals("eliminó"))||(oracion.equals("perder"))||(oracion.equals("perdio"))||(oracion.equals("perdimos"))||(oracion.equals("eliminamos"))||(oracion.equals("elimino"))||(oracion.equals("pierdo"))||(oracion.equals("perder"))) {
   
             auxe="resta";
                return auxe;
                }
         
         
           else
         if  ((oracion.equals("segmentacion"))||(oracion.equals("Segmentación"))||(oracion.equals("Desmembramiento"))||(oracion.equals("desmembramiento"))||(oracion.equals("fragmentación"))||(oracion.equals("Fragmentación"))||(oracion.equals("Parte"))||(oracion.equals("parte"))||(oracion.equals("partición"))||(oracion.equals("fraccionar"))||(oracion.equals("fraccionamiento"))||(oracion.equals("subdivision"))||(oracion.equals("Subdivision"))||(oracion.equals("division"))||(oracion.equals("fragmento"))||(oracion.equals("Fragmento"))||(oracion.equals("Fragmentó"))||(oracion.equals("parte"))||(oracion.equals("rebajo"))||(oracion.equals("divide"))||(oracion.equals(" entre "))||(oracion.equals("dividió"))) {
   
             auxe="dividir";
                return auxe;
                }
         
         
             else
         if  ((oracion.equals("multiplica"))||(oracion.equals("multiplicación"))||(oracion.equals("en cada"))||(oracion.equals("multiplicado"))||(oracion.equals("multiplicando"))||(oracion.equals(" por "))||(oracion.equals("redoblar"))||(oracion.equals("elevar"))||(oracion.equals("eleva"))||(oracion.equals("propaga"))||(oracion.equals("propagar"))||(oracion.equals("reproducir"))||(oracion.equals("reproduce"))) {
   
             auxe="multiplicar";
                return auxe;
                }
         
         
         
         
         else
//             ||(oracion.equals("sustrajo"))||(oracion.equals("robo"))||(oracion.equals("Robo"))||(oracion.equals("Robó"))||(oracion.equals("rompio"))||(oracion.equals("rompieron"))||(oracion.equals("rompió"))||(oracion.equals("detraer"))||(oracion.equals("rebajar"))||(oracion.equals("rebajo"))||(oracion.equals("quito"))||(oracion.equals("aminoró"))||(oracion.equals("aminoro"))||(oracion.equals("redujo"))||(oracion.equals("sustrajo"))||(oracion.equals("sustraer"))||(oracion.equals("eliminar"))||(oracion.equals("eliminó"))||(oracion.equals("perder"))||(oracion.equals("perdio"))||(oracion.equals("perdimos"))||(oracion.equals("eliminamos"))||(oracion.equals("elimino"))||(oracion.equals("pierdo"))||(oracion.equals("perder"))
         if  ((oracion.equals("vector"))||(oracion.equals("constante"))||(oracion.equals("variable"))||(oracion.equals("Variable"))||(oracion.equals("Constantes"))||(oracion.equals("atributo"))||(oracion.equals("Atributo"))||(oracion.equals("variables"))||(oracion.equals("vectores"))) {
   
             auxe="vector";
                return auxe;
                }
         
         
             
         
         
         
         
         else{
         auxe="NS";
         
         }
          return auxe;
         
    }

    private boolean comprobarpaalbrautilizandolivshtein(String palabra) {
       

 boolean variable=false;
 int aux=0;
 
        if (palabra.length()>4) {
            
            
            List<String> listad=listadodeoperaiconesdetectadas();
            for (int i = 0; i < listad.size(); i++) {
                aux= LevenshteinDistance.computeLevenshteinDistance(listad.get(i), palabra);
                System.out.println(palabra+" " + " " + listad.get(i) +" "+ aux);
                
                if (aux<3) {
                    variable=true;
                    
                }
              
            }
            
            
            
        }
        return variable;

//To change body of generated methods, choose Tools | Templates.
    }

    private void generarAleatorio() {
     
       double aux=0.0;
        
        for (int i = 0; i < 10; i++) {
            
             double numero =  (Math.random() );
                    System.out.println("p="+numero);
            
        }
        
        
    }
    
    
        
}
