/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package EasyCompiler.SintaxAnalyzer;

/**
 *
 * @author tomaso
 */

import EasyCompiler.LexicalAnalyzer.*;
import EasyCompiler.Errors.*;
import EasyCompiler.*;
import java.io.*;
import java.util.LinkedList;

/* Easy Gramar in Backus–Naur Form (BNF)
     * 
     * <program> ::= program id; <var> <body>
     * <var> ::= [var <declarations>]
     * <declarations> ::= [<declaration> <declarations>]
     * <declaration> ::= <variables> : <type>;
     * <variables> ::= id <others_variables>
     * <others_variables> ::= [, <variables>]
     * <type> ::= int | float | bool
     * <body> ::= <instructions_block>
     * <instructions_block> :== begin <instructions_list> end
     * <instructions_list> ::= [<single_instruction> <instructions_list>]
     * <single_instruction> ::= <read> | <write> | <assignment> | <if_then_else> | <while_do> | ; 
     * <read> ::= read id;
     * <write> ::= write <expression>;
     * <assignment> ::= id = <expression>;
     * <expression> ::= <single_expression><others_singles_expressions>
     * <others_singles_expressions> ::= [<operators_level_0> <single_expression> <others_singles_expressions>]
     * <operators_level_0> ::= > | < | >= | <= | != | ==
     * <single_expression> ::= <term> <others_terms>
     * <others_terms> ::= [<operators_level_1> <term> <others_terms>]
     * <operators_level_1> ::= + | - | "||"
     * <term> ::= <factor> <others_factors>
     * <others_factors> ::= [<operators_level_2> <factor> <others_factors>]
     * <operators_level_2> ::= * | / | % | &&
     * <factor> ::= int_literal | float_literal | <bool_literal> | id | ! <factor> | <signo> <factor> | (<expression>)
     * <bool_literal> ::= true | false
     * <signo> ::= + | -
     * <if_then_else> ::= if <expression> then <instructions>[else <instructions>]
     * <instructions> ::= <instructions_block> | <single_instruction>
     * <while_do> ::= while <expression> do <instructions>
     * 
     */

public class Parser {
    
    private Scanner scanner;
    private ErrorReporter errorReporter;
    private Token currentToken;
     private LinkedList<String>operation;
    private void ReportSyntacticError(String text) 
    {
        errorReporter.add(new SyntacticError(currentToken.getPosition(), text));
    }
    
    private void Match(TokenKind tokenExpected)
    {
        if(currentToken.getKind() == tokenExpected)
            AcceptIt();
        else
            ReportSyntacticError(tokenExpected.toString() + " tokenExpected");
    }
    
    private void AcceptIt()
    {
        currentToken = scanner.scan();
    }
    
    public Parser(Scanner scanner, ErrorReporter errorReporter)
    {
        this.scanner = scanner;
        this.errorReporter = errorReporter;
    }
    
    public void Parse()
    {
        currentToken = scanner.scan();
        ParseProgram();
        
        //para que lea todos los posibles tokens y darlos en la salida del scanner
        while(currentToken.getKind() != TokenKind.EOT)
        {
            ReportSyntacticError(currentToken.getLexeme() + " not expected after end of program");
            currentToken = scanner.scan();
        }  
    }

    private void ParseProgram()
    {
        //<program> ::= program id; <var> <body>
      //  Match(TokenKind.Program);
        SourcePosition position = currentToken.getPosition();
        Match(TokenKind.Id);
        Match(TokenKind.SemiColon);
        ParseVar();
        ParseBody();
    }
    
    private void ParseVar() 
    {
        //<var> ::= [var <declarations>]
        if(currentToken.getKind() == TokenKind.Var) 
        {
            AcceptIt();
            ParseDeclarations();
        }
    }

    private void ParseDeclarations()
    {
        //<declarations> ::= [<declaration> <declarations>]
        if(currentToken.getKind() == TokenKind.Id) 
        {
            ParseDeclaration();
            ParseDeclarations();
        }
    }

    private void ParseDeclaration()
    {
        //<declaration> ::= <variables> : <type>;
        ParseVariables();
        Match(TokenKind.Colon);
        ParseType();
        Match(TokenKind.SemiColon);
    }

    private void ParseType() 
    {
        //<type> ::= int | float | bool
        if(IsType(currentToken.getKind()))
            AcceptIt();
        else
            ReportSyntacticError("Type expected");
    }

    private EasyTypes GetType(TokenKind tokenKind)
    {
        switch(tokenKind)
        {
            case Bool : return EasyTypes.Bool;
            case Int : return EasyTypes.Int;
            case Float : return EasyTypes.Float;
            default: throw new CurrentTokenIsNoTypeException(tokenKind);
        }
    }

    private boolean IsType(TokenKind tokenKind)
    {
        return tokenKind == TokenKind.Int || tokenKind == TokenKind.Float || tokenKind == TokenKind.Bool;
    }

    private void ParseVariables()
    {
        //<variables> ::= id <others_variables>
        Match(TokenKind.Id);
        ParseOthersVariables();
    }

    private void ParseOthersVariables()
    {
        //<others_variables> ::= [, <variables>]
        if(currentToken.getKind() == TokenKind.Comma) 
        {
            Match(TokenKind.Comma);
            ParseVariables();
        }
    }

    private void ParseBody()
    {
        //<body> ::= <instructions_block>
        ParseInstructionsBlock();
    }

    private void ParseInstructionsBlock()
    {
        //<instructions_block> ::= begin <instructions_list> end
        Match(TokenKind.Begin);
        ParseInstructionsList();
        Match(TokenKind.End);
    }

    private void ParseInstructionsList()
    {
        //<instructions_list> ::= [<single_instruction> <instructions_list>]
        if(currentToken.getKind() == TokenKind.Write ||
            currentToken.getKind() == TokenKind.Read ||
            currentToken.getKind() == TokenKind.Id ||
            currentToken.getKind() == TokenKind.If ||
            currentToken.getKind() == TokenKind.While ||
            currentToken.getKind() == TokenKind.SemiColon)
        {
            ParseSingleInstruction();
            ParseInstructionsList();
        }
    }

    private void ParseSingleInstruction()
    {
        //<single_instruction> ::= <read> | <write> | <assignment> | <if_then_else> | <while_do> | ; 
        switch(currentToken.getKind()) 
        {
//            case Read:
//            {
//                ParseRead();
//                break;
//            }    
//            case Write:
//            {    
//                ParseWrite();
//                break;
//            }
//            case Id:
//            {
//                ParseAssigment();
//                break;
//            }
//            case If:
//            {
//                ParseIfThenElse();
//                break;
//            }
//            case While:
//            {
//                ParseWhileDo();
//                break;
//            }
//            case SemiColon:
//            {
//                Match(TokenKind.SemiColon);
//                break;
//            }
            
             case Sum:
           {
               ParseSum();
//                ParseRead();
//                break;
            }    
           
              case Minus:
           {
               ParseMinus();
//                ParseRead();
//                break;
            } 
              case Multiplication:
           {
               ParseMultiplica();
//                ParseRead();
//                break;
            } 
            
           
           
              case Division:
           {
               ParseDivisi();
//                ParseRead();
//                break;
            } 
     
                   case vector:
           {
               ParseVector();
//                ParseRead();
//                break;
            } 
 
  
        }
    }

    private void ParseSum()
    {
        //<read> ::= read id;
        Match(TokenKind.Read);
        Match(TokenKind.Id);
        Match(TokenKind.SemiColon);
    }
    
    
    
        private void ParseMinus()
    {
        //<read> ::= read id;
        Match(TokenKind.Read);
        Match(TokenKind.Id);
        Match(TokenKind.SemiColon);
    }
    
    private void ParseMultiplica()
    {
        //<read> ::= read id;
        Match(TokenKind.Read);
        Match(TokenKind.Id);
        Match(TokenKind.SemiColon);
    }
    
            private void ParseDivisi()
    {
        //<read> ::= read id;
        Match(TokenKind.Read);
        Match(TokenKind.Id);
        Match(TokenKind.SemiColon);
    }
    
            
    private void ParseVector()
    {
        //<read> ::= read id;
        Match(TokenKind.Read);
        Match(TokenKind.Id);
        Match(TokenKind.SemiColon);
    }
        private void ParseRead()
    {
        //<read> ::= read id;
        Match(TokenKind.Read);
        Match(TokenKind.Id);
        Match(TokenKind.SemiColon);
    }
        
        

    private void ParseWrite()
    {
        //<write> ::= write <expression>;
        Match(TokenKind.Write);
        ParseExpression();
        Match(TokenKind.SemiColon);
    }

    private void ParseAssigment()
    {
        //<assignment> ::= id = <expression>;
        Match(TokenKind.Id);
        Match(TokenKind.Assignment);
        ParseExpression();
        Match(TokenKind.SemiColon);
    }

    private void ParseExpression()
    {
        //<expression> ::= <single_expression><others_singles_expressions>
        ParseSingleExpression();
        ParseOthersSinglesExpressions();   
    }

    private void ParseOthersSinglesExpressions()
    {
        //<others_singles_expressions> ::= [<operators_level_0> <expression>]
        //<operators_level_0> ::= > | < | >= | <= | != | ==
        TokenKind kind = currentToken.getKind();
                
        if(kind == TokenKind.GreaterThan || 
           kind == TokenKind.GreaterThanOrEqual ||
           kind == TokenKind.LessThan || 
           kind == TokenKind.LessThanOrEqual ||
           kind == TokenKind.Equality || 
           kind == TokenKind.Inequality)
        {
            Match(kind);
            ParseExpression();
        }
    }

    private void ParseSingleExpression()
    {
        //<single_expression> ::= <term> <others_terms>
        ParseTerm();
        ParseOthersTerms();
    }

    private void ParseOthersTerms()
    {
        //<others_terms> ::= [<operators_level_1> <single_expression>]
        //<operators_level_1> ::= + | - | "||"
        TokenKind kind = currentToken.getKind();
        
        if (kind == TokenKind.Sum || 
            kind == TokenKind.Minus ||
            kind == TokenKind.Or)
        {
            Match(kind);
            ParseSingleExpression();
        }
    }

    private void ParseTerm()
    {
        //<term> ::= <factor> <others_factors>
        ParseFactor();
        ParseOthersFactors();
    }

    private void ParseOthersFactors()
    {
        //<others_factors> ::= [<operators_level_2> <term>]
        //<operators_level_2> ::= * | / | % | &&
        TokenKind kind = currentToken.getKind();
        
        if (kind == TokenKind.Multiplication || 
            kind == TokenKind.Division||
            kind == TokenKind.Modulus || 
            kind == TokenKind.And)
        {
            Match(kind);
            ParseTerm();
        }
    }

    private void ParseFactor()
    {
        //<factor> ::= int_literal | float_literal | <bool_literal> | id | ! <factor> | <sign> <factor> | (<expression>)
        //<bool_literal> ::= true | false
        //<sign> ::= + | -
        switch(currentToken.getKind())
        {
            case IntLiteral:
            case FloatLiteral:
            case BoolLiteral:
            case Id:
                AcceptIt();
                break;
            case Not:
            case Minus:
            case Sum:
                AcceptIt();
                ParseFactor();
                break;
            case LeftParen:
                AcceptIt();
                ParseExpression();
                Match(TokenKind.RigthParen);
                break;
            default:
                ReportSyntacticError("Wrong expression");
                break;
        }
    }

    private void ParseIfThenElse()
    {
        //<if_then_else> ::= if <expression> then <instructions>[else <instructions>]
        Match(TokenKind.If);
        ParseExpression();
        Match(TokenKind.Then);
        ParseInstructions();
        if(currentToken.getKind() == TokenKind.Else) 
        {
            AcceptIt();
            ParseInstructions();
        }
     }

     private void ParseInstructions()
     {
        //<instructions> ::= <instructions_block> | <single_instruction>
        if (currentToken.getKind() == TokenKind.Begin)
            ParseInstructionsBlock();
        else
            ParseSingleInstruction();
     }

     private void ParseWhileDo()
     {
        //<while_do> ::= while <expression> do <instructions>
        SourcePosition position = currentToken.getPosition();
        Match(TokenKind.While);
        ParseExpression();
        Match(TokenKind.Do);
        ParseInstructions();
     }
     
     
       
}