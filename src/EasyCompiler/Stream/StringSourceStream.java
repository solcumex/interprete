/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package EasyCompiler.Stream;

import EasyCompiler.LexicalAnalyzer.Scanner;
import java.io.*;
/**
 *
 * @author trujillo
 */
public class StringSourceStream extends SourceStream {
    
    public StringSourceStream(String sourceCode)
    {
      
        super();
        this.cadeString=sourceCode;
        this.textReader = new StringReader(sourceCode);
    }
}
