/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package EasyCompiler.LexicalAnalyzer;

/**
 *
 * @author DDC Programación, UCI
 */
import java.util.Hashtable;
import java.io.*;
import java.util.LinkedList;

import EasyCompiler.SourcePosition;
import EasyCompiler.Stream.*;
import EasyCompiler.SymbolsTable.*;
import EasyCompiler.Errors.*;
import EasyCompiler.Modelos.Operacion;
import EasyCompiler.Semantic.AnalisisSentencia;
import EasyCompiler.Semantic.bitrigramas;
import EasyCompiler.Semantic.markov;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

enum ScannerState {
    Start, BeginToken, Id, NumLiteral, BeginFloatLiteral, FloatLiteral, Colon, SemiColon, Comma, LeftParen,
    RigthParen, Sum, Minus, Multiplication, Division, Modulus, Equality, Assignment, Not, Inequality, LessThan,
    GreaterThan, GreaterThanOrEqual, LessThanOrEqual, BeginAnd, BeginOr, And, Or, EOT, Error
}

public class Scanner {

    private SourceStream sourceStream;
    private char currentChar;
    private String lexeme;
    private Hashtable<String, TokenKind> keywordsTable;
    private SymbolsTable symbolsTable;
    private ErrorReporter errorReporter;
    private LinkedList<Token> output;
    public bitrigramas markovclase;
    public List<String> CadenaTexto;
    ArrayList operaList = new ArrayList();
    
    public List<Operacion> listadoOperaciones = new ArrayList<>();

    public ArrayList getOperaList() {
        return operaList;
    }

    private boolean isLetter(char c) {
        return (Character.isLetter(c) || c == '_');
    }

    private boolean isDigit(char c) {
        return Character.isDigit(c);
    }

    private boolean isSeparator(char c) {
        switch (c) {
            case ' ':
            case '\n':
            case '\r':
            case '\t':
                return true;
            default:
                return false;
        }
    }

    private void takeIt() {
        try {
            lexeme = lexeme + Character.toString(currentChar);
            currentChar = sourceStream.Read();
        } catch (Exception e) {
            throw new ScanningProblemException(e);
        }
    }

    private void skipIt() {
        try {
            currentChar = sourceStream.Read();
        } catch (Exception e) {
            throw new ScanningProblemException(e);
        }
    }

    private void fillKeywordsTable() {
        keywordsTable.put("begin", TokenKind.Begin);
        keywordsTable.put("bool", TokenKind.Bool);
        keywordsTable.put("true", TokenKind.BoolLiteral);
        keywordsTable.put("false", TokenKind.BoolLiteral);
        keywordsTable.put("end", TokenKind.End);
        keywordsTable.put("float", TokenKind.Float);
        keywordsTable.put("int", TokenKind.Int);
        keywordsTable.put("program", TokenKind.Program);
        keywordsTable.put("read", TokenKind.Read);
        keywordsTable.put("var", TokenKind.Var);
        keywordsTable.put("write", TokenKind.Write);
        keywordsTable.put("if", TokenKind.If);
        keywordsTable.put("then", TokenKind.Then);
        keywordsTable.put("else", TokenKind.Else);
        keywordsTable.put("while", TokenKind.While);
        keywordsTable.put("vector", TokenKind.vector);
        keywordsTable.put("suma", TokenKind.Sum);
        keywordsTable.put("mas", TokenKind.Sum);
        keywordsTable.put("sumar", TokenKind.Sum);
        keywordsTable.put("sumo", TokenKind.Sum);
        keywordsTable.put("Adición", TokenKind.Sum);
        keywordsTable.put("adición", TokenKind.Sum);
        keywordsTable.put("adicion", TokenKind.Sum);
        keywordsTable.put("más", TokenKind.Sum);
        keywordsTable.put("juntar", TokenKind.Sum);
        keywordsTable.put("junto", TokenKind.Sum);
        keywordsTable.put("congrego", TokenKind.Sum);
        keywordsTable.put("Agregar", TokenKind.Sum);
        keywordsTable.put("agregaron", TokenKind.Sum);
        keywordsTable.put("agregar", TokenKind.Sum);
        keywordsTable.put("agrega", TokenKind.Sum);
        keywordsTable.put("agrego", TokenKind.Sum);
        keywordsTable.put("poner", TokenKind.Sum);
        keywordsTable.put("pongo", TokenKind.Sum);
        keywordsTable.put("adiciono", TokenKind.Sum);
        keywordsTable.put("uno", TokenKind.Sum);
        keywordsTable.put("Aditamento", TokenKind.Sum);
        keywordsTable.put("aditamento", TokenKind.Sum);
        keywordsTable.put("Complemento", TokenKind.Sum);
        keywordsTable.put("complemento", TokenKind.Sum);
        keywordsTable.put("Añadidura", TokenKind.Sum);
        keywordsTable.put("añadidura", TokenKind.Sum);
        keywordsTable.put("añado", TokenKind.Sum);
        keywordsTable.put("más", TokenKind.Sum);
        keywordsTable.put("mas", TokenKind.Sum);
        keywordsTable.put("puso", TokenKind.Sum);
        keywordsTable.put("puse", TokenKind.Sum);
        keywordsTable.put("sumatoria", TokenKind.Sum);

        keywordsTable.put("di", TokenKind.Sum);
        keywordsTable.put("dar", TokenKind.Sum);
        keywordsTable.put("compró", TokenKind.Sum);
        keywordsTable.put("comprar", TokenKind.Sum);
        keywordsTable.put("unir", TokenKind.Sum);

        keywordsTable.put("dio", TokenKind.Sum);
        keywordsTable.put("unió", TokenKind.Sum);
        keywordsTable.put("ganó", TokenKind.Sum);
        keywordsTable.put("gana", TokenKind.Sum);
        keywordsTable.put("une", TokenKind.Sum);
        keywordsTable.put("ganar", TokenKind.Sum);
        keywordsTable.put("gano", TokenKind.Sum);
        keywordsTable.put("incremento", TokenKind.Sum);
        keywordsTable.put("incrementar", TokenKind.Sum);
        keywordsTable.put("incrementó", TokenKind.Sum);

        keywordsTable.put("adicion", TokenKind.Sum);
        keywordsTable.put("adicionar", TokenKind.Sum);
        keywordsTable.put("adicionando", TokenKind.Sum);

        keywordsTable.put("resta", TokenKind.Minus);
        keywordsTable.put("Resta", TokenKind.Minus);
        keywordsTable.put("resto", TokenKind.Minus);
        keywordsTable.put("disminuir", TokenKind.Minus);
        keywordsTable.put("quitar", TokenKind.Minus);
        keywordsTable.put("romper", TokenKind.Minus);
        keywordsTable.put("rompieron", TokenKind.Minus);
        keywordsTable.put("sustraer", TokenKind.Minus);
        keywordsTable.put("rompió", TokenKind.Minus);
        keywordsTable.put("sustrajo", TokenKind.Minus);
        keywordsTable.put("robo", TokenKind.Minus);
        keywordsTable.put("rompieron", TokenKind.Minus);
        keywordsTable.put("rompio", TokenKind.Minus);
        keywordsTable.put("detraer", TokenKind.Minus);
        keywordsTable.put("rebajar", TokenKind.Minus);
        keywordsTable.put("rebajo", TokenKind.Minus);
        keywordsTable.put("quito", TokenKind.Minus);
        keywordsTable.put("aminoró", TokenKind.Minus);
        keywordsTable.put("redujo", TokenKind.Minus);
        keywordsTable.put("sustrajo", TokenKind.Minus);
        keywordsTable.put("Eliminar", TokenKind.Minus);
        keywordsTable.put("perdio", TokenKind.Minus);
        keywordsTable.put("eliminó", TokenKind.Minus);
        keywordsTable.put("elimino", TokenKind.Minus);
        keywordsTable.put("perder", TokenKind.Minus);

        keywordsTable.put("redoblar", TokenKind.Sum);
        keywordsTable.put("incrementar", TokenKind.Sum);
        keywordsTable.put("redoblar", TokenKind.Sum);
        keywordsTable.put("redoblar", TokenKind.Sum);

        keywordsTable.put("constante", TokenKind.vector);
        keywordsTable.put("vectores", TokenKind.vector);

        keywordsTable.put("do", TokenKind.Do);
    }

    private TokenKind defineTokenKind(String lexeme) {
        TokenKind kind;
        if (keywordsTable.containsKey(lexeme)) {
            kind = keywordsTable.get(lexeme);
            return kind;
        }
        return TokenKind.Id;
    }

    public Scanner(SourceStream source, SymbolsTable symbolsTable, ErrorReporter errorReporter) throws IOException {

        markovclase = bitrigramas.getSingletonInstance();

        this.sourceStream = source;
        this.symbolsTable = symbolsTable;
        this.errorReporter = errorReporter;
        this.output = new LinkedList<Token>();
        currentChar = source.Read();
        lexeme = new String("");
        keywordsTable = new Hashtable<String, TokenKind>();
        fillKeywordsTable();
    }

    public Token scan() {
        TokenKind kind = TokenKind.Error;
        ScannerState state = ScannerState.Start;
        boolean scaning = true;
        lexeme = "";
        while (scaning) {

            switch (state) {
                case Start:
                    if (isSeparator(currentChar)) {
                        skipIt();
                    } else {
                        state = ScannerState.BeginToken;
                    }
                    break;
                case BeginToken:
                    if (isLetter(currentChar)) {
                        takeIt();
                        state = ScannerState.Id;
                    } else if (isDigit(currentChar)) {
                        takeIt();
                        state = ScannerState.NumLiteral;
                    } else if (currentChar == ':') {
                        takeIt();
                        state = ScannerState.Colon;
                    } else if (currentChar == ';') {
                        takeIt();
                        state = ScannerState.SemiColon;
                    } else if (currentChar == ',') {
                        takeIt();
                        state = ScannerState.Comma;
                    } else if (currentChar == '(') {
                        takeIt();
                        state = ScannerState.LeftParen;
                    } else if (currentChar == ')') {
                        takeIt();
                        state = ScannerState.RigthParen;
                    } else if (currentChar == '+') {
                        takeIt();
                        state = ScannerState.Sum;
                    } else if (currentChar == '-') {
                        takeIt();
                        state = ScannerState.Minus;
                    } else if (currentChar == '*') {
                        takeIt();
                        state = ScannerState.Multiplication;
                    } else if (currentChar == '/') {
                        takeIt();
                        state = ScannerState.Division;
                    } else if (currentChar == '%') {
                        takeIt();
                        state = ScannerState.Modulus;
                    } else if (currentChar == '=') {
                        takeIt();
                        state = ScannerState.Assignment;
                    } else if (currentChar == '!') {
                        takeIt();
                        state = ScannerState.Not;
                    } else if (currentChar == '<') {
                        takeIt();
                        state = ScannerState.LessThan;
                    } else if (currentChar == '>') {
                        takeIt();
                        state = ScannerState.GreaterThan;
                    } else if (currentChar == '&') {
                        takeIt();
                        state = ScannerState.BeginAnd;
                    } else if (currentChar == '|') {
                        takeIt();
                        state = ScannerState.BeginOr;
                    } else if (currentChar == '\0') {
                        state = ScannerState.EOT;
                    } else {
                        takeIt();
                        state = ScannerState.Error;
                    }
                    break;
                case Id:
                    if (isLetter(currentChar) || isDigit(currentChar)) {
                        takeIt();
                    } else {
                        scaning = false;
                        kind = TokenKind.Id;
                    }
                    break;
                case NumLiteral:
                    if (isDigit(currentChar)) {
                        takeIt();
                    } else if (currentChar == '.') {
                        takeIt();
                        state = ScannerState.BeginFloatLiteral;
                    } else {
                        scaning = false;
                        kind = TokenKind.IntLiteral;
                    }
                    break;
                case BeginFloatLiteral:
                    if (isDigit(currentChar)) {
                        takeIt();
                        state = ScannerState.FloatLiteral;
                    } else {
                        state = ScannerState.Error;
                    }
                    break;
                case FloatLiteral:
                    if (isDigit(currentChar)) {
                        takeIt();
                    } else {
                        scaning = false;
                        kind = TokenKind.FloatLiteral;
                    }
                    break;
                case Colon:
                    scaning = false;
                    kind = TokenKind.Colon;
                    break;
                case SemiColon:
                    scaning = false;
                    kind = TokenKind.SemiColon;
                    break;
                case Comma:
                    scaning = false;
                    kind = TokenKind.Comma;
                    break;
                case LeftParen:
                    scaning = false;
                    kind = TokenKind.LeftParen;
                    break;
                case RigthParen:
                    scaning = false;
                    kind = TokenKind.RigthParen;
                    break;
                case Sum:
                    scaning = false;
                    kind = TokenKind.Sum;
                    break;
                case Minus:
                    scaning = false;
                    kind = TokenKind.Minus;
                    break;
                case Multiplication:
                    scaning = false;
                    kind = TokenKind.Multiplication;
                    break;
                case Division:
                    scaning = false;
                    kind = TokenKind.Division;
                    break;
                case Modulus:
                    scaning = false;
                    kind = TokenKind.Modulus;
                    break;
                case Assignment:
                    if (currentChar == '=') {
                        takeIt();
                        state = ScannerState.Equality;
                    } else {
                        scaning = false;
                        kind = TokenKind.Assignment;
                    }
                    break;
                case Equality:
                    scaning = false;
                    kind = TokenKind.Equality;
                    break;
                case Not:
                    if (currentChar == '=') {
                        takeIt();
                        state = ScannerState.Inequality;
                    } else {
                        scaning = false;
                        kind = TokenKind.Not;
                    }
                    break;
                case Inequality:
                    scaning = false;
                    kind = TokenKind.Inequality;
                    break;
                case LessThan:
                    if (currentChar == '=') {
                        takeIt();
                        state = ScannerState.LessThanOrEqual;
                    } else {
                        scaning = false;
                        kind = TokenKind.LessThan;
                    }
                    break;
                case LessThanOrEqual:
                    scaning = false;
                    kind = TokenKind.LessThanOrEqual;
                    break;
                case GreaterThan:
                    if (currentChar == '=') {
                        takeIt();
                        state = ScannerState.GreaterThanOrEqual;
                    } else {
                        scaning = false;
                        kind = TokenKind.GreaterThan;
                    }
                    break;
                case GreaterThanOrEqual:
                    scaning = false;
                    kind = TokenKind.GreaterThanOrEqual;
                    break;
                case BeginAnd:
                    if (currentChar == '&') {
                        takeIt();
                        state = ScannerState.And;
                    } else {
                        state = ScannerState.Error;
                    }
                    break;
                case And:
                    scaning = false;
                    kind = TokenKind.And;
                    break;
                case BeginOr:
                    if (currentChar == '|') {
                        takeIt();
                        state = ScannerState.Or;
                    } else {
                        state = ScannerState.Error;
                    }
                    break;
                case Or:
                    scaning = false;
                    kind = TokenKind.Or;
                    break;
                case Error:
                    scaning = false;
                    kind = TokenKind.Error;
                    break;
                case EOT:
                    scaning = false;
                    kind = TokenKind.EOT;
                    break;
            }
        }
        int line = sourceStream.getCurrentLine();
        SourcePosition position = new SourcePosition(sourceStream.getCurrentPosition() - lexeme.length(), sourceStream.getCurrentPosition() - 1, line);

        if (kind == TokenKind.Error) {
            errorReporter.add(new LexicalError(position, "Unexpected character '" + lexeme + "'"));
            output.add(new Token(TokenKind.Error, lexeme, position));
            return scan();
        }

        Token currentToken;

        if (kind == TokenKind.Id) {
            kind = defineTokenKind(lexeme);
        }

        if (kind == TokenKind.Id || kind == TokenKind.BoolLiteral || kind == TokenKind.IntLiteral || kind == TokenKind.FloatLiteral) {
            currentToken = new Token(kind, lexeme, position, symbolsTable.add(lexeme, kind));
        } else {
            currentToken = new Token(kind, lexeme, position);
        }

        output.add(currentToken);

        return currentToken;
    }

    public LinkedList<Token> getCurrentOutput() {
        conocerOperacion(output);
        return output;
    }

    public Integer[] ordenarlista(ArrayList<Integer> listado) {

        Integer[] arreglo = new Integer[listado.size()];
        for (int i = 0; i < listado.size(); i++) {
            arreglo[i] = listado.get(i);
        }

        Arrays.sort(arreglo);
        return arreglo;
    }

    public void calcularoperacionsuma(ArrayList<Integer> listadeenteros, Operacion oepracion) {

        String auxevariable = listadeenteros.get(0).toString();
        int resultado = listadeenteros.get(0);

        for (int j = 1; j < listadeenteros.size(); j++) {

            auxevariable = auxevariable + "+" + listadeenteros.get(j);
            resultado = resultado + listadeenteros.get(j);

        }

        oepracion.setCadenaoperacion(auxevariable);
        oepracion.setResultado(String.valueOf(resultado));
        operaList.add(oepracion);
        oepracion.getOperaResult();
    }

    public void calcularoperacionresta(ArrayList<Integer> listadeenteros, Operacion oepracion) {
        Integer[] valoresordenado = ordenarlista(listadeenteros);
        String auxevariable = valoresordenado[valoresordenado.length - 1].toString();
        int resultado = valoresordenado[valoresordenado.length - 1];

        for (int j = valoresordenado.length - 2; j >= 0; j--) {

            auxevariable = auxevariable + "-" + valoresordenado[j];
            resultado = resultado - valoresordenado[j];

        }

        oepracion.setCadenaoperacion(auxevariable);
        oepracion.setResultado(String.valueOf(resultado));
        operaList.add(oepracion);
        oepracion.getOperaResult();

    }

    public void calcularoperacionmultiplicar(ArrayList<Integer> listadeenteros, Operacion oepracion) {
        String auxevariable = listadeenteros.get(0).toString();
        int resultado = listadeenteros.get(0);

        for (int j = 1; j < listadeenteros.size(); j++) {

            auxevariable = auxevariable + "*" + listadeenteros.get(j);
            resultado = resultado * listadeenteros.get(j);

        }

        oepracion.setCadenaoperacion(auxevariable);
        oepracion.setResultado(String.valueOf(resultado));
        operaList.add(oepracion);
        oepracion.getOperaResult();

    }

    public void calcularoperaciondividir(ArrayList<Integer> listadeenteros, Operacion oepracion) {
  Integer[] valoresordenado = ordenarlista(listadeenteros);
                    String auxevariable = valoresordenado[valoresordenado.length - 1].toString();
                    int resultado = valoresordenado[valoresordenado.length - 1];

                    for (int j = valoresordenado.length - 2; j >= 0; j--) {

                        auxevariable = auxevariable + "/" + valoresordenado[j];
                        resultado = resultado / valoresordenado[j];

                    }

                    oepracion.setCadenaoperacion(auxevariable);
                    oepracion.setResultado(String.valueOf(resultado));
                    operaList.add(oepracion);
                    oepracion.getOperaResult();
    }

    public void calcularoperacionvector(ArrayList<Integer> listadeenteros, Operacion oepracion) {
        
        
         String auxevariable = "vector =";
        int resultado = 0;

        for (int j = 0; j < listadeenteros.size(); j++) {

            auxevariable = auxevariable  + listadeenteros.get(j);
            resultado = resultado + listadeenteros.get(j);

        }

        oepracion.setCadenaoperacion(auxevariable);
        oepracion.setResultado(" ");
        operaList.add(oepracion);
        oepracion.getOperaResult();

    }

    public void conocerOperacion(LinkedList<Token> listatoken) {

        Operacion oepracion;
        ArrayList<Integer> listadeenteros;
        String cadenacompleta = sourceStream.getCadeString();
        String[] arregloespacio = cadenacompleta.split("\r\n");
        CadenaTexto = new ArrayList<>();
//        System.out.println(sourceStream.getCadeString()+"tutoriales");

        ArrayList<Integer> lineas = new ArrayList<>();

        Map<Integer, List<Token>> cantidad = new HashMap<>();

        LinkedList<Token> listatokenporlinea = new LinkedList<>();

        int lineavalor = 1;
        for (int i = 0; i < listatoken.size(); i++) {

            if (lineavalor == listatoken.get(i).getPosition().getLine()) {

                listatokenporlinea.add(listatoken.get(i));

            } else {
                cantidad.put(lineavalor, listatokenporlinea);
                lineavalor = listatoken.get(i).getPosition().getLine();
                listatokenporlinea = new LinkedList<>();
            }

        }
        cantidad.put(listatoken.getLast().getPosition().getLine(), listatokenporlinea);

        List<Token> listatokenAuxe;
        Iterator it = cantidad.keySet().iterator();

        //poner cadena completa y reducirla al maximo
        AnalisisSentencia analisisdista = new AnalisisSentencia(cadenacompleta);
         String cadenanueva = analisisdista.devolverOracionporbasura();
       TreeMap<String,ArrayList<Integer>>listadooperacionevalores= analisisdista.dividirsentenciasPorOperaciones(cadenanueva);
         oepracion = new Operacion();    
        if (analisisdista.getOperaciones().isEmpty()) {
            
            
        oepracion.setCadenaoperacion("No Existe ninguna Operacion");
        oepracion.setResultado(" ");
        operaList.add(oepracion);
        oepracion.getOperaResult();
            
            
        }else
        {
        
        
       
        System.out.println(cadenanueva);
        
        
        
        
        
        for (Map.Entry<String,ArrayList<Integer>> entry : listadooperacionevalores.entrySet()) {
    System.out.println("clave=" + entry.getKey() + ", valor=" + entry.getValue());

        
        String operation=entry.getKey();
        
        
        listadeenteros = entry.getValue();

        ArrayList<String> operadoresoperaciones = analisisdista.getOperaciones();

   

                if (operation.equals("suma")) {

                    calcularoperacionsuma(listadeenteros, oepracion);

                }

                if (operation.equals("resta")) {

                    calcularoperacionresta(listadeenteros, oepracion);

                }

//               
                if (operation.equals("multiplicar")) {

                    calcularoperacionmultiplicar(listadeenteros, oepracion);
                }

                if (operation.equals("dividir")) {

                  calcularoperaciondividir(listadeenteros, oepracion);

                }
                
                    if (operation.equals("vector")) {

                  calcularoperacionvector(listadeenteros, oepracion);

                }
                

            
} }
//            }
//             
        }

    }


