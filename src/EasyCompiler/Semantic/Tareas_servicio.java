/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EasyCompiler.Semantic;

import EasyCompiler.Connection.Conexion;
import EasyCompiler.Modelos.OracionNumeracion;
import EasyCompiler.Modelos.Tarea;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultListModel;

/**
 *
 * @author Fernando
 */
public class Tareas_servicio {

    private final String tabla = "tareas";
    private final String tareasba = "tareaba";
    private final String bigrama = "bigramas";
    private final String tareaba4 = "tareaba4";
    private final String trigrama = "trigrama";
    private final String information = "information";
    private final String cantpalaclavevalor = "cantpalaclavevalor";
    private final String cantpalaclavevalorbasura = "cantpalaclavevalorbasura";
    
    private final String mapacaracteres = "mapacaracteres";
    private final String mapacaracteres3 = "mapacaracteres3";
    private final String mapacaracteres4 = "mapacaracteres4";
    private final String oracionnumeracion = "oracionnumeracion";
    private final String oracionnumeracion3 = "oracionnumeracion3";
    private final String oracionnumeracion4 = "oracionnumeracion4";
    private final String tablahmm = "tablahmm";
    private final String tareabatra = "tareabatra";
    private final String tareabatranueva = "tareabatranueva";
    private final String matrizbjk = "matrizbjk";
    private final String jobposts = "jobposts";

    public void guardar(Connection conexion, Tarea tarea) throws SQLException {
        try {
            PreparedStatement consulta;
            if (tarea.getId_tarea() == null) {
                consulta = conexion.prepareStatement("INSERT INTO " + this.tabla + "(titulo, descripcion, nivel_de_prioridad) VALUES(?, ?, ?)");
                consulta.setString(1, tarea.getTitulo());
                consulta.setString(2, tarea.getDescripcion());
                consulta.setInt(3, tarea.getNivel_de_prioridad());
            } else {
                consulta = conexion.prepareStatement("UPDATE " + this.tabla + " SET titulo = ?, descripcion = ?, nivel_de_prioridad = ? WHERE id_tarea = ?");
                consulta.setString(1, tarea.getTitulo());
                consulta.setString(2, tarea.getDescripcion());
                consulta.setInt(3, tarea.getNivel_de_prioridad());
                consulta.setInt(4, tarea.getId_tarea());
            }
            consulta.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    public Tarea recuperarPorId(Connection conexion, int id_tarea) throws SQLException {
        Tarea tarea = null;
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT titulo, descripcion, nivel_de_prioridad,operacion FROM " + this.tabla + " WHERE id_tarea = ?");
            consulta.setInt(1, id_tarea);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                tarea = new Tarea(id_tarea, resultado.getString("titulo"), resultado.getString("descripcion"), resultado.getInt("nivel_de_prioridad"), resultado.getString("operacion"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
        return tarea;
    }

    public void eliminar(Connection conexion, Tarea tarea) throws SQLException {
        try {
            PreparedStatement consulta = conexion.prepareStatement("DELETE FROM " + this.tabla + " WHERE id_tarea = ?");
            consulta.setInt(1, tarea.getId_tarea());
            consulta.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
    }

    public List<Tarea> recuperarTodas(Connection conexion) throws SQLException {
        List<Tarea> tareas;
        tareas = new ArrayList<Tarea>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT id_tarea, titulo, descripcion, nivel_de_prioridad,operacion FROM " + this.tabla + " ORDER BY nivel_de_prioridad");
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                tareas.add(new Tarea(resultado.getInt("id_tarea"), resultado.getString("titulo"), resultado.getString("descripcion"), resultado.getInt("nivel_de_prioridad"), resultado.getString("operacion")));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
        return tareas;
    }

    public List<String> recuperarSetencias(Connection conexion) throws SQLException {
        List<String> corpustext = new ArrayList<String>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT  descripcion FROM " + this.tabla);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                corpustext.add(resultado.getString("descripcion"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        return corpustext;
    }

    public ArrayList<String> recuperarSetenciasparacuadrar(Connection conexion) throws SQLException {
        ArrayList<String> corpustext = new ArrayList<String>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT  numreducida FROM " + this.tareabatranueva + "  where operacion = \"dividir\" LIMIT 300");
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                corpustext.add(resultado.getString("numreducida"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        return corpustext;
    }

    public List<String> recuperarSetenciasbasura(Connection conexion) throws SQLException {
        List<String> corpustext = new ArrayList<String>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT  basura FROM " + this.tareabatranueva);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                corpustext.add(resultado.getString("basura"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        return corpustext;
    }

    public List<String> recuperarSetenciasbasuraID(Connection conexion) throws SQLException {
        List<String> corpustext = new ArrayList<String>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT  id_tarea FROM " + this.tareabatranueva);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                corpustext.add(resultado.getString("id_tarea"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        return corpustext;
    }

    public List<String> recuperarSetenciasbasura4(Connection conexion) throws SQLException {
        List<String> corpustext = new ArrayList<String>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT  basura FROM " + this.tareaba4);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                corpustext.add(resultado.getString("basura"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        return corpustext;
    }

    public List<String> recuperarSetenciassuma(Connection conexion) throws SQLException {
        List<String> corpustext = new ArrayList<String>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT  descripcion FROM " + this.tabla + "  where operacion = 'suma' ");
            System.out.println(consulta.toString());
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                corpustext.add(resultado.getString("descripcion"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        return corpustext;
    }

    public List<String> recuperarSetenciasResta(Connection conexion) throws SQLException {
        List<String> corpustext = new ArrayList<String>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT  descripcion FROM " + this.tabla + "  where operacion = 'resta' ");
            System.out.println(consulta.toString());
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                corpustext.add(resultado.getString("descripcion"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        return corpustext;
    }

    public List<String> recuperarSetenciasMult(Connection conexion) throws SQLException {
        List<String> corpustext = new ArrayList<String>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT  descripcion FROM " + this.tabla + "  where operacion = 'multiplicar' ");
            System.out.println(consulta.toString());
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                corpustext.add(resultado.getString("descripcion"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        return corpustext;
    }

    public List<String> recuperarSetenciasDivi(Connection conexion) throws SQLException {
        List<String> corpustext = new ArrayList<String>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT  descripcion FROM " + this.tabla + "  where operacion = 'dividir' ");
            System.out.println(consulta.toString());
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                corpustext.add(resultado.getString("descripcion"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        return corpustext;
    }

    public List<String> recuperarSetenciasid(Connection conexion) throws SQLException {
        List<String> corpustext = new ArrayList<String>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT  id_tarea FROM " + this.tabla);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                corpustext.add(resultado.getString("id_tarea"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        return corpustext;
    }

    public List<String> recuperarSetenciastitulo(Connection conexion) throws SQLException {
        List<String> corpustext = new ArrayList<String>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT  titulo FROM " + this.tabla);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                corpustext.add(resultado.getString("titulo"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        return corpustext;
    }

    public List<String> recuperarSetenciasnivel_de_prioridad(Connection conexion) throws SQLException {
        List<String> corpustext = new ArrayList<String>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT  nivel_de_prioridad FROM " + this.tabla);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                corpustext.add(resultado.getString("nivel_de_prioridad"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        return corpustext;
    }

    public List<String> recuperarSetenciasoperacion(Connection conexion) throws SQLException {
        List<String> corpustext = new ArrayList<String>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT  operacion FROM " + this.tabla);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                corpustext.add(resultado.getString("operacion"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        return corpustext;
    }

    public void guardarenTareasBa(Connection conexion, int id_tarea, String titulo, String descripcion, int nivelprioridad, String operacion, String basura) throws SQLException {
        PreparedStatement consulta = conexion.prepareStatement("INSERT INTO " + this.tareabatranueva + "(id_tarea,titulo,descripcion,nivel_de_prioridad,operacion,basura) VALUES(?, ?, ?, ?,?,?)");

        consulta.setInt(1, id_tarea);
        consulta.setString(2, titulo);
        consulta.setString(3, descripcion);
        consulta.setInt(4, nivelprioridad);
        consulta.setString(5, operacion);
        consulta.setString(6, basura);
        System.out.println(consulta);

        consulta.executeUpdate();

    }

    public void guardarenTareasBacon4(Connection conexion, int id_tarea, String titulo, String descripcion, int nivelprioridad, String operacion, String basura) throws SQLException {
        PreparedStatement consulta = conexion.prepareStatement("INSERT INTO " + this.tareaba4 + "(id_tarea,titulo,descripcion,nivel_de_prioridad,operacion,basura) VALUES(?, ?, ?, ?,?,?)");

        consulta.setInt(1, id_tarea);
        consulta.setString(2, titulo);
        consulta.setString(3, descripcion);
        consulta.setInt(4, nivelprioridad);
        consulta.setString(5, operacion);
        consulta.setString(6, basura);
        System.out.println(consulta);

        consulta.executeUpdate();

    }

    public List<String> recuperarSetenciasOperacion(Connection conexion, int num) throws SQLException {
        List<String> corpustext = new ArrayList<String>();
        try {
            String auxe = "SELECT  descripcion FROM " + this.tabla + " WHERE nivel_de_prioridad=" + num;
            PreparedStatement consulta = conexion.prepareStatement(auxe);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                corpustext.add(resultado.getString("descripcion"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        return corpustext;
    }

    public void guardarBrigramas(Connection conexion, Map<String, Integer> bigramCounts) throws SQLException {

        PreparedStatement consulta = conexion.prepareStatement("INSERT INTO " + this.bigrama + "(clave, valor, descripcion,cantidad) VALUES(?, ?, ?, ?)");

        Iterator it = bigramCounts.keySet().iterator();

        while (it.hasNext()) {
            String key = (String) it.next();
            consulta.setString(1, key);
            consulta.setString(2, bigramCounts.get(key).toString());
            consulta.setString(3, "Bigramas");
            consulta.setInt(4, bigramCounts.get(key).toString().length());
            System.out.println(consulta);
            consulta.executeUpdate();

            String aux = "Clave: " + key + " -> Valor: " + bigramCounts.get(key);
//            modelo.addElement(aux);         

        }

    }

    public void actualizarOperacionesdetareas(Connection conexion, int valor, String operacion) throws SQLException {
        PreparedStatement consulta;
        //  UPDATE `tareas` SET `operacion` = 'vector' WHERE `tareas`.`id_tarea` = 4;

        consulta = conexion.prepareStatement("UPDATE " + this.tabla + " SET operacion = ? WHERE id_tarea = ?");
        consulta.setString(1, operacion);
        consulta.setInt(2, valor);
        consulta.executeUpdate();
//                consulta.setInt(3, tarea.getNivel_de_prioridad());
//                consulta.setInt(4, tarea.getId_tarea());

    }

    public void actualizarTareas(Connection conexion, int valor, String operacion) throws SQLException {
        PreparedStatement consulta;
        //  UPDATE `tareas` SET `operacion` = 'vector' WHERE `tareas`.`id_tarea` = 4;

        consulta = conexion.prepareStatement("UPDATE " + this.tabla + " SET descripcion = ? WHERE id_tarea = ?");
        consulta.setString(1, operacion);
        consulta.setInt(2, valor);
        consulta.executeUpdate();
//                consulta.setInt(3, tarea.getNivel_de_prioridad());
//                consulta.setInt(4, tarea.getId_tarea());

    }

    public void guardaractualizarinformacion(Connection conexion, Map<String, Integer> mapa) throws SQLException {
//       String var=  INSERT INTO `information` (`id`, `cantpalabra`, `cantoracionesdiferetes`, `cantpalabrasdiferentes`, `cantpalabrasuma`, `cantpalabrasumadifere`, `cantidesetenciassuma`, `cantpalabrasresta`, `cantpalabrasrestadiferente`, `cantidasddesetenciasresta`, `cantidadpalabrasmult`, `cantidaddepalabrasmultdiferen`, `setenciasmulti`, `cantdivisionpalabra`, `cantdividiferentepala`, `cantsetenciasdiv`) VALUES (NULL, '10', '10', '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)";

        PreparedStatement consulta = conexion.prepareStatement("INSERT INTO " + this.information + "(cantpalabra, cantoracionesdiferetes, cantpalabrasdiferentes,cantpalabrasuma,cantpalabrasumadifere,cantidesetenciassuma, cantpalabrasresta, cantpalabrasrestadiferente, cantidasddesetenciasresta, cantidadpalabrasmult, cantidaddepalabrasmultdiferen, setenciasmulti, cantdivisionpalabra, cantdividiferentepala, cantsetenciasdiv) VALUES (?,?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?) ");

        int valores = mapa.get("cantpalabrasdiferentes");
        consulta.setInt(1, mapa.get("cantpalabra"));
        consulta.setInt(2, mapa.get("cantoracionesdiferetes"));
        consulta.setInt(3, valores);
        consulta.setInt(4, 0);
        consulta.setInt(5, 0);
        consulta.setInt(6, 0);
        consulta.setInt(7, 0);
        consulta.setInt(8, 0);

        consulta.setInt(9, 0);
        consulta.setInt(10, 0);
        consulta.setInt(11, 0);
        consulta.setInt(12, 0);

        consulta.setInt(13, 0);
        consulta.setInt(14, 0);
        consulta.setInt(1, 0);

//             consulta.setString(2, bigramCounts.get(key).toString());             
//             consulta.setString(3, "trigrama");
//             consulta.setInt(4,bigramCounts.get(key).toString().length() );
        System.out.println(consulta);
        consulta.executeUpdate();

    }

    public void guardaractualizarinformaciontareas(Connection conexion, Map<String, Integer> mapa) throws SQLException {
//       String var=  INSERT INTO `information` (`id`, `cantpalabra`, `cantoracionesdiferetes`, `cantpalabrasdiferentes`, `cantpalabrasuma`, `cantpalabrasumadifere`, `cantidesetenciassuma`, `cantpalabrasresta`, `cantpalabrasrestadiferente`, `cantidasddesetenciasresta`, `cantidadpalabrasmult`, `cantidaddepalabrasmultdiferen`, `setenciasmulti`, `cantdivisionpalabra`, `cantdividiferentepala`, `cantsetenciasdiv`) VALUES (NULL, '10', '10', '10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)";

        //PreparedStatement   consulta = conexion.prepareStatement("INSERT INTO " + this.information + "(cantpalabra, cantoracionesdiferetes, cantpalabrasdiferentes,cantpalabrasuma,cantpalabrasumadifere,cantidesetenciassuma, cantpalabrasresta, cantpalabrasrestadiferente, cantidasddesetenciasresta, cantidadpalabrasmult, cantidaddepalabrasmultdiferen, setenciasmulti, cantdivisionpalabra, cantdividiferentepala, cantsetenciasdiv) VALUES (?,?, ?, ?, ?, ?,?, ?, ?, ?, ?, ?, ?, ?, ?) ");
        PreparedStatement consulta = conexion.prepareStatement("UPDATE " + this.information + " SET cantdivisionpalabra = ?, cantdividiferentepala = ?, cantsetenciasdiv = ? ");
        int valores = mapa.get("cantpalabrasdiferentes");

        consulta.setInt(1, mapa.get("cantpalabra"));
        consulta.setInt(2, valores);
        consulta.setInt(3, mapa.get("cantoracionesdiferetes"));
//               consulta.setInt(4, 0);
//                   consulta.setInt(5, 0);
//              consulta.setInt(6,0);
//               consulta.setInt(7, 0);
//               consulta.setInt(8, 0);
//               
//               
//                   consulta.setInt(9,0);
//              consulta.setInt(10, 0);
//               consulta.setInt(11,0);
//               consulta.setInt(12, 0);
//               
//                   consulta.setInt(13,0);
//              consulta.setInt(14,0);
//                consulta.setInt(1,0);
//               

//             consulta.setString(2, bigramCounts.get(key).toString());             
//             consulta.setString(3, "trigrama");
//             consulta.setInt(4,bigramCounts.get(key).toString().length() );
        System.out.println(consulta);
        consulta.executeUpdate();

    }

    public void trigramaBrigramas(Connection conexion, Map<String, Integer> bigramCounts) throws SQLException {

        PreparedStatement consulta = conexion.prepareStatement("INSERT INTO " + this.bigrama + "(clave, valor, descripcion,cantidad) VALUES(?, ?, ?, ?)");

        Iterator it = bigramCounts.keySet().iterator();
        int contador = 0;

        while (it.hasNext()) {
            String key = (String) it.next();
            consulta.setString(1, key);
            consulta.setString(2, bigramCounts.get(key).toString());
            consulta.setString(3, "bigrama");
            consulta.setInt(4, bigramCounts.get(key).toString().length());
            System.out.println(contador++ + " " + consulta);
            consulta.executeUpdate();

            String aux = "Clave: " + key + " -> Valor: " + bigramCounts.get(key);
//            modelo.addElement(aux);         

        }

    }

    public void GuardarWordcount(Map<String, Integer> wordCounts) throws SQLException, ClassNotFoundException {
        Connection conexion = Conexion.obtener();
        PreparedStatement consulta = conexion.prepareStatement("INSERT INTO " + this.cantpalaclavevalorbasura + "(clave, valor, descripcion) VALUES(?, ?, ?)");

        Iterator it = wordCounts.keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            consulta.setString(1, key);
            consulta.setString(2, wordCounts.get(key).toString());
            consulta.setString(3, "conteo");
//             consulta.setInt(4,wordCounts.get(key).toString().length() );
            System.out.println(consulta);
            consulta.executeUpdate();

//            String aux= "Clave: " + key + " -> Valor: " + wordCounts.get(key);            
//            modelo.addElement(aux);         
        }

    }

    void GuardarMapaCaracteres(Map<String, Integer> mapaDeCaracteres) throws SQLException, ClassNotFoundException {
        Connection conexion = Conexion.obtener();
        PreparedStatement consulta = conexion.prepareStatement("INSERT INTO " + this.mapacaracteres + "(clave, valor, descripcion) VALUES(?, ?, ?)");

        Iterator it = mapaDeCaracteres.keySet().iterator();
        int cont = 0;
        while (it.hasNext()) {
            String key = (String) it.next();
            consulta.setString(1, key);
            consulta.setString(2, mapaDeCaracteres.get(key).toString());
            consulta.setString(3, "Numero de cada palabra diferente");
//             consulta.setInt(4,wordCounts.get(key).toString().length() );
            System.out.println(cont++ + " " + consulta);
            consulta.executeUpdate();

//            String aux= "Clave: " + key + " -> Valor: " + wordCounts.get(key);            
//            modelo.addElement(aux);         
        }

    }

    void GuardarMapaCaracteres3(Map<String, Integer> mapaDeCaracteres) throws SQLException, ClassNotFoundException {
        Connection conexion = Conexion.obtener();
        PreparedStatement consulta = conexion.prepareStatement("INSERT INTO " + this.mapacaracteres3 + "(clave, valor, descripcion) VALUES(?, ?, ?)");

        Iterator it = mapaDeCaracteres.keySet().iterator();
        int cont = 0;
        while (it.hasNext()) {
            String key = (String) it.next();
            consulta.setString(1, key);
            consulta.setString(2, mapaDeCaracteres.get(key).toString());
            consulta.setString(3, "Numero de cada palabra diferente");
//             consulta.setInt(4,wordCounts.get(key).toString().length() );
            System.out.println(cont++ + " " + consulta);
            consulta.executeUpdate();

//            String aux= "Clave: " + key + " -> Valor: " + wordCounts.get(key);            
//            modelo.addElement(aux);         
        }

    }

    void GuardarMapaCaracteres4(Map<String, Integer> mapaDeCaracteres) throws SQLException, ClassNotFoundException {
        Connection conexion = Conexion.obtener();
        PreparedStatement consulta = conexion.prepareStatement("INSERT INTO " + this.mapacaracteres4 + "(clave, valor, descripcion) VALUES(?, ?, ?)");

        Iterator it = mapaDeCaracteres.keySet().iterator();
        int cont = 0;
        while (it.hasNext()) {
            String key = (String) it.next();
            consulta.setString(1, key);
            consulta.setString(2, mapaDeCaracteres.get(key).toString());
            consulta.setString(3, "Numero de cada palabra diferente");
//             consulta.setInt(4,wordCounts.get(key).toString().length() );
            System.out.println(cont++ + " " + consulta);
            consulta.executeUpdate();

//            String aux= "Clave: " + key + " -> Valor: " + wordCounts.get(key);            
//            modelo.addElement(aux);         
        }

    }

    void GuardarAsignacionpalabra(ArrayList<String> auxiliarMOstrarNumeros, List<String> corpusText) throws SQLException, ClassNotFoundException {

        Connection conexion = Conexion.obtener();
        PreparedStatement consulta = conexion.prepareStatement("INSERT INTO " + this.oracionnumeracion + "(oracion, numeracion , descripcion) VALUES(?, ?, ?)");

        for (int i = 0; i < corpusText.size(); i++) {

//         Iterator it = auxiliarMOstrarNumeros.keySet().iterator();
//          while(it.hasNext()){
//            String key =  (String) it.next();
            consulta.setString(1, corpusText.get(i));
            consulta.setString(2, auxiliarMOstrarNumeros.get(i));
            consulta.setString(3, "Oracion en texto y numero");
//             consulta.setInt(4,wordCounts.get(key).toString().length() );
            System.out.println(consulta);
            consulta.executeUpdate();

//            String aux= "Clave: " + key + " -> Valor: " + wordCounts.get(key);            
//            modelo.addElement(aux);         
//                              }
        }

    }

    void GuardarAsignacionpalabra3(ArrayList<String> auxiliarMOstrarNumeros, List<String> corpusText) throws SQLException, ClassNotFoundException {

        Connection conexion = Conexion.obtener();
        PreparedStatement consulta = conexion.prepareStatement("INSERT INTO " + this.oracionnumeracion3 + "(oracion, numeracion , descripcion) VALUES(?, ?, ?)");

        for (int i = 0; i < corpusText.size(); i++) {

//         Iterator it = auxiliarMOstrarNumeros.keySet().iterator();
//          while(it.hasNext()){
//            String key =  (String) it.next();
            consulta.setString(1, corpusText.get(i));
            consulta.setString(2, auxiliarMOstrarNumeros.get(i));
            consulta.setString(3, "Oracion en texto y numero");
//             consulta.setInt(4,wordCounts.get(key).toString().length() );
            System.out.println(consulta);
            consulta.executeUpdate();

//            String aux= "Clave: " + key + " -> Valor: " + wordCounts.get(key);            
//            modelo.addElement(aux);         
//                              }
        }

    }

    void GuardarAsignacionpalabra4(ArrayList<String> auxiliarMOstrarNumeros, List<String> corpusText) throws SQLException, ClassNotFoundException {

        Connection conexion = Conexion.obtener();
        PreparedStatement consulta = conexion.prepareStatement("INSERT INTO " + this.oracionnumeracion4 + "(oracion, numeracion , descripcion) VALUES(?, ?, ?)");

        for (int i = 0; i < corpusText.size(); i++) {

//         Iterator it = auxiliarMOstrarNumeros.keySet().iterator();
//          while(it.hasNext()){
//            String key =  (String) it.next();
            consulta.setString(1, corpusText.get(i));
            consulta.setString(2, auxiliarMOstrarNumeros.get(i));
            consulta.setString(3, "Oracion en texto y numero");
//             consulta.setInt(4,wordCounts.get(key).toString().length() );
            System.out.println(consulta);
            consulta.executeUpdate();

//            String aux= "Clave: " + key + " -> Valor: " + wordCounts.get(key);            
//            modelo.addElement(aux);         
//                              }
        }

    }

    public List<String> obtenerbigramas(Connection conexion) throws SQLException {

        List<String> tareas;
        tareas = new ArrayList<String>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT clave FROM " + this.cantpalaclavevalor);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                tareas.add(resultado.getString("clave"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
        return tareas;
    }

    public List<String> obtenervalordepalabras(Connection conexion) throws SQLException {

        List<String> tareas;
        tareas = new ArrayList<String>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT clave FROM " + this.cantpalaclavevalor + " where valor >8 ");
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                tareas.add(resultado.getString("clave"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
        return tareas;
    }

    public void recuperarTodasGuardarentablaHMM(Connection conexion) throws SQLException {
        List<Tarea> tareas;
        tareas = new ArrayList<Tarea>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT id_tarea, titulo, descripcion, nivel_de_prioridad,operacion FROM " + this.tabla);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                tareas.add(new Tarea(resultado.getInt("id_tarea"), resultado.getString("titulo"), resultado.getString("descripcion"), resultado.getInt("nivel_de_prioridad"), resultado.getString("operacion")));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        List<OracionNumeracion> OracionNumeracion;
        OracionNumeracion = new ArrayList<OracionNumeracion>();

        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT * FROM " + this.oracionnumeracion);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                OracionNumeracion.add(new OracionNumeracion(resultado.getString("oracion"), resultado.getString("numeracion"), resultado.getString("descripcion")));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        List<OracionNumeracion> OracionNumeracion3;
        OracionNumeracion3 = new ArrayList<OracionNumeracion>();

        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT * FROM " + this.oracionnumeracion3);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                OracionNumeracion3.add(new OracionNumeracion(resultado.getString("oracion"), resultado.getString("numeracion"), resultado.getString("descripcion")));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        List<OracionNumeracion> OracionNumeracion4;
        OracionNumeracion4 = new ArrayList<OracionNumeracion>();

        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT * FROM " + this.oracionnumeracion4);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                OracionNumeracion4.add(new OracionNumeracion(resultado.getString("oracion"), resultado.getString("numeracion"), resultado.getString("descripcion")));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        PreparedStatement consulta = conexion.prepareStatement("INSERT INTO " + this.tablahmm + "(clave , clavecon3 , clavecon4,valor,descripcion,numeracion,numeracion3,numeracion4) VALUES(?, ?, ?,?, ?, ?,?,?)");

        for (int i = 0; i < tareas.size(); i++) {

//         Iterator it = auxiliarMOstrarNumeros.keySet().iterator();
//          while(it.hasNext()){
//            String key =  (String) it.next();
            consulta.setString(1, tareas.get(i).getDescripcion());
            consulta.setString(2, OracionNumeracion3.get(i).getOracion());
            consulta.setString(3, OracionNumeracion4.get(i).getOracion());
            consulta.setString(4, tareas.get(i).getTitulo());
            consulta.setString(5, tareas.get(i).getOperacion());
            consulta.setString(6, OracionNumeracion.get(i).getNumeracion());
            consulta.setString(7, OracionNumeracion3.get(i).getNumeracion());
            consulta.setString(8, OracionNumeracion4.get(i).getNumeracion());
//             consulta.setInt(4,wordCounts.get(key).toString().length() );
            System.out.println(consulta);
            consulta.executeUpdate();

            // tareas;
        }

    }

    void guardarmatrizasignacion(String valores) throws SQLException, ClassNotFoundException {
        Connection conexion = Conexion.obtener();

        PreparedStatement consulta = conexion.prepareStatement("INSERT INTO " + this.matrizbjk + "(clave,valor , descripcion) VALUES(?, ?, ?)");

//         Iterator it = auxiliarMOstrarNumeros.keySet().iterator();
//          while(it.hasNext()){
//            String key =  (String) it.next();
        consulta.setString(1, "Valor =3");
        //consulta.setString(2, auxiliarMOstrarNumeros.get(i));             
        consulta.setString(2, valores);
        consulta.setString(3, "3");
//             consulta.setInt(4,wordCounts.get(key).toString().length() );
        // System.out.println(consulta);
        consulta.executeUpdate();

//            String aux= "Clave: " + key + " -> Valor: " + wordCounts.get(key);            
//            modelo.addElement(aux);         
//                              }
    }

    void actualizarAsignacionpalabra4(ArrayList<String> auxiliarMOstrarNumeros, List<String> corpusText, List<String> corpusTextss) throws SQLException, ClassNotFoundException {
        Connection conexion = Conexion.obtener();

        PreparedStatement consulta;
        consulta = conexion.prepareStatement("UPDATE " + this.tareabatranueva + " SET numeracion = ? WHERE id_tarea = ? ");
        for (int i = 0; i < corpusText.size(); i++) {

//         Iterator it = auxiliarMOstrarNumeros.keySet().iterator();
//          while(it.hasNext()){
//            String key =  (String) it.next();
            //   consulta.setString(1, corpusText.get(i));
            consulta.setString(1, auxiliarMOstrarNumeros.get(i));
            consulta.setInt(2, Integer.parseInt(corpusTextss.get(i)));
//             consulta.setInt(4,wordCounts.get(key).toString().length() );
            System.out.println(consulta);
            consulta.executeUpdate();

//            String aux= "Clave: " + key + " -> Valor: " + wordCounts.get(key);            
//            modelo.addElement(aux);         
//                              }
        }

    }

    public void reducirnumeracionBS() throws SQLException, ClassNotFoundException {

        Connection conexion = Conexion.obtener();
        List<String> corpustext = new ArrayList<String>();
        List<String> corpustextid = new ArrayList<String>();
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT  numeracion, id_tarea FROM " + this.tareabatranueva);
            ResultSet resultado = consulta.executeQuery();
            while (resultado.next()) {
                corpustext.add(resultado.getString("numeracion"));
                corpustextid.add(resultado.getString("id_tarea"));
            }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }

        PreparedStatement consulta;

        for (int i = 0; i < corpustext.size(); i++) {

            String campo = reducir42(corpustext.get(i));

            consulta = conexion.prepareStatement("UPDATE " + this.tareabatranueva + " SET numreducida = ? WHERE id_tarea = ?");
            consulta.setString(1, campo);
            consulta.setString(2, corpustextid.get(i));
            System.out.println(campo);

            consulta.executeUpdate();

        }

    }

    private String reducir42(String get) {

        String valor = "";

        String[] cadena = get.split(",");

        for (int i = 0; i < cadena.length; i++) {

            if (!"62".equals(cadena[i])) {

                valor = valor + cadena[i] + " ";

            } else {

                if ("62".equals(cadena[i]) && i == 0) {

                    valor = valor + cadena[i] + " ";

                }
                if ("62".equals(cadena[i]) && i > 0) {

                    if (!"62".equals(cadena[i - 1])) {

                        valor = valor + cadena[i] + " ";

                    }
                }
            }

        }

        return valor;

    }

    public void guardarmapapublicaciones(Map<String, Map<String, String>> mapeador) throws ClassNotFoundException, SQLException {
        
        List <List<String>> listad= generarPost(mapeador);
        int identificador=806;
        
        
        Connection conexion = Conexion.obtener();
           PreparedStatement consulta = conexion.prepareStatement("INSERT INTO " + this.jobposts + "(country_code, user_id, company_id,category_id,post_type_id,company_name,company_description,logo,title,description,tags,salary_min,salary_max,salary_type_id,negotiable,start_date,application_url,contact_name,email,phone,phone_hidden,address,city_id,lon,lat,ip_addr,visits,email_token,phone_token,tmp_token,verified_email,verified_phone,reviewed,featured,archived,partner,created_at,updated_at,deleted_at,id) VALUES(?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?, ?, ?, ?,?, ?,  ?,?)");
         
            
           for (int i = 0; i < listad.size(); i++) {
              
               
                consulta.setString(1, "MX");
            consulta.setInt(2, 6);
            consulta.setInt(3, 1);
            consulta.setInt(4, 8);
             consulta.setInt(5, 2);             
            consulta.setString(6,  listad.get(i).get(5));          
            
            consulta.setString(7, "Sin ddescripcion");
            consulta.setString(8, "files/mx/1/a1dc0d6473f7b6427b03dd588e73c6d4.png");
            
            
            consulta.setString(9, listad.get(i).get(2));
              consulta.setString(10, listad.get(i).get(3));  
              
              consulta.setString(11, "etiqueta");
            consulta.setDouble(12,  Double.parseDouble(listad.get(i).get(1)));
            consulta.setDouble(13, Double.parseDouble(listad.get(i).get(1)));
            consulta.setInt(14, 3);
             consulta.setString(15, null);
               consulta.setString(16, null);
                   consulta.setString(17, null);
                   
            
            consulta.setString(18,"Ernesto Carlos");
             consulta.setString(19, "info@solcumex.com");
             
             
             
             
             
                   
            consulta.setString(20, "+526142723345");
            consulta.setInt(21, 1);
            consulta.setString(22,null);
             consulta.setInt(23,4014338);           
                              consulta.setDouble(24,  28.6353);
            consulta.setDouble(25, 106.089);
            consulta.setString(26, "187.189.17.209");
            consulta.setInt(27, 44);
             consulta.setString(28, null);
             
             
                     consulta.setString(29, null);
            consulta.setString(30,null);
            consulta.setInt(31, 1);
            consulta.setInt(32, 1);
            
            
            
             consulta.setInt(33, 0);
             
                    
            consulta.setInt(34, 0);
            consulta.setInt(35, 0);
            consulta.setString(36, null);
             consulta.setString(37,"2018-11-30 10:07:53");
              consulta.setString(38, "2018-12-09 01:52:03");
                consulta.setString(39, null);
                consulta.setInt(40, identificador);
                
                System.out.println(consulta);
             
             
            consulta.executeUpdate();
               
            identificador++;
        }
//        Iterator it = bigramCounts.keySet().iterator();
//        int contador = 0;
//
//        while (it.hasNext()) {
//            String key = (String) it.next();
//            consulta.setString(1, key);
//            consulta.setString(2, bigramCounts.get(key).toString());
//            consulta.setString(3, "bigrama");
//            consulta.setInt(4, bigramCounts.get(key).toString().length());
//            System.out.println(contador++ + " " + consulta);
//            consulta.executeUpdate();
//
//            String aux = "Clave: " + key + " -> Valor: " + bigramCounts.get(key);
////            modelo.addElement(aux);         
//
//        }

        
    }

    private List<List<String>> generarPost(Map<String, Map<String, String>> mapeador) {
        List<List<String>> listad= new ArrayList<>();
        
        
        for (Map.Entry<String, Map<String, String>> entry : mapeador.entrySet()) {
            List<String> guardardatos= new ArrayList<>();
            
    System.out.println("clave=" + entry.getKey() + ", valor=" + entry.getValue());
     
        String aplicationurl=entry.getKey();
        
        
            if (entry.getValue().isEmpty()) {
                continue;
            }
        Map<String,String>efecto=entry.getValue();
        
//         for (Map.Entry<String,String> entrys :efecto.entrySet()) {
             
             String descripcion= "Detalle de la oferta"+ efecto.get("Detalle de la Oferta");
             
             String salarioneto =efecto.get("Salario neto mensual:");
             String salarionetodecuerpo ="Salario neto mensual:"+ efecto.get("Salario neto mensual:");
               String ubicacion= "Ubicación:"+ efecto.get("Ubicación:");             
             String tipocontrato ="Tipo de contrato:"+ efecto.get("Tipo de contrato:");
             String vigenciadeoferta ="Vigencia de la oferta:"+ efecto.get("Vigencia de la oferta:");
             String diaslaborales ="Días laborales:"+ efecto.get("Días laborales:");
             String horariodetrabajo ="Horario de trabajo:"+ efecto.get("Horario de trabajo:");
             String rolarturno ="Rolar turnos:"+ efecto.get("Rolar turnos:");
             String estudissolicitado ="Estudios Solicitados:"+ efecto.get("Estudios Solicitados:");
             String competenciastransversales ="Competencias transversales:"+ efecto.get("Competencias transversales:");
                String idioma ="Idiomas:"+ efecto.get("Idiomas:");
                String prestacipones ="Prestaciones:"+ efecto.get("Prestaciones:");
                    String numerodeplaza ="Número de plazas:"+ efecto.get("Número de plazas:");
                        String funcionesactividadesarealizar ="Funciones y actividades a realizar"+ efecto.get("Funciones y actividades a realizar");             
                            String observaciones ="Observaciones"+ efecto.get("Observaciones");
                            
                            String[] division=descripcion.split("-");
                            
                            
             String cuerpo=descripcion+"<br\\>"+salarionetodecuerpo+"<br\\>"+ubicacion+"<br\\>"+tipocontrato+"<br\\>"+vigenciadeoferta+"<br\\>"+diaslaborales+"<br\\>"+horariodetrabajo+"<br\\>"+rolarturno+"<br\\>"+estudissolicitado+"<br\\>"+competenciastransversales+"<br\\>"+idioma+"<br\\>"+prestacipones+"<br\\>"+numerodeplaza+"<br\\>"+funcionesactividadesarealizar+"<br\\>"+observaciones;
             
             guardardatos.add(aplicationurl);
             salarioneto=salarioneto.replace(",", "");
             guardardatos.add(salarioneto.substring(salarioneto.indexOf("$")+"$".length(),salarioneto.indexOf(".")));
             guardardatos.add(descripcion);
             guardardatos.add(cuerpo);
             guardardatos.add(division[0]);
             
             if (division.length==2) {
                 guardardatos.add(division[1]);
            }else
             {
                  guardardatos.add("ninguna");
             }
            
             
                            
             
             listad.add(guardardatos);
             
             
//         }
         
         }
        
   
        
        return listad;
    }

    public String BuscarInformacion(String string) throws SQLException, ClassNotFoundException {     
           Connection conexion = Conexion.obtener();
           String resul="";
        
        
        //Tarea tarea = null;
        try {
            PreparedStatement consulta = conexion.prepareStatement("SELECT clave FROM " + this.cantpalaclavevalorbasura + " WHERE id = ?");
            consulta.setString(1, string);
            ResultSet resultado = consulta.executeQuery();
            
            
            
            while (resultado.next()) {
               resul=resultado.getString("clave");
                    }
        } catch (SQLException ex) {
            throw new SQLException(ex);
        }
        return resul;
        
    }
    
    
    
    
    
    

}
