/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EasyCompiler.Semantic;

import EasyCompiler.Connection.Conexion;
import EasyCompiler.Modelos.Tarea;
import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

/**
 *
 * @author letal
 */
public class AnalisisSentencia {

    private String oracion;
    ArrayList<Integer> listadeenteros;
    ArrayList<String> operaciones;
    HashMap<Integer, String> mapadeoperaciones;
    HashMap<String, ArrayList<Integer>> operacionesyenteros;
    ArrayList<Integer> lisposicionestempo = new ArrayList<>();
     TreeMap<String, ArrayList<Integer>> auxe ;

    public AnalisisSentencia(String Oracion) {

        oracion = Oracion;
        listadeenteros = new ArrayList<>();
        operaciones = new ArrayList<>();
        mapadeoperaciones = new HashMap<>();
        operacionesyenteros = new HashMap<>();
    }

    public ArrayList<Integer> getListadeenteros() {
        return listadeenteros;
    }

    public ArrayList<String> getOperaciones() {
        return operaciones;
    }

    public String devolverOracionporbasura() {
//         HashMap<Integer,String>mapa= new HashMap<>();
//           HashMap<Integer,String>mapasuma= new HashMap<>();
//             HashMap<Integer,String>maparesta= new HashMap<>();
//             HashMap<Integer,String>mapamult= new HashMap<>();
//               HashMap<Integer,String>mapadivisi= new HashMap<>();
//                     HashMap<Integer,String>mapadvari= new HashMap<>();
//                           HashMap<Integer,String>mapaNS= new HashMap<>();

        String aux = "";
        String Oracionbasuradevuelta = "";
        // List<String>corpus=this.tareas_servicio.recuperarSetencias(Conexion.obtener());
        //   List<String>corpusid=this.tareas_servicio.recuperarSetenciasid(Conexion.obtener());
        oracion = trimsetencias();
        String auxevar = "";

        String[] corpus = oracion.split(" ");
        int posicion = 0;

        for (int i = 0; i < corpus.length; i++) {
            auxevar = comprobarOperacion(corpus[i]);

            if (auxevar.equals("suma") || (auxevar.equals("resta")) || (auxevar.equals("dividir")) || (auxevar.equals("divididas"))|| (auxevar.equals("multiplicar")) || (auxevar.equals("vector"))) {
                Oracionbasuradevuelta = Oracionbasuradevuelta + auxevar + " ";
                operaciones.add(auxevar);

                mapadeoperaciones.put(i, auxevar);
                lisposicionestempo.add(i);

                continue;
            }
            if (isNumeric(corpus[i])) {

                Oracionbasuradevuelta = Oracionbasuradevuelta + corpus[i] + " ";

                listadeenteros.add(Integer.parseInt(corpus[i]));
                continue;
            } else {
                Oracionbasuradevuelta = Oracionbasuradevuelta + "BS" + " ";
            }

            System.out.println(" Oración original: " + oracion + "   La Oracion basura es  " + Oracionbasuradevuelta);

        }

        return Oracionbasuradevuelta;
    }

    public TreeMap<String, ArrayList<Integer>> dividirsentenciasPorOperaciones(String oraciones) {

        int valor = 0;
         auxe = new TreeMap<>();
       
        ArrayList<String> opera = operaciones;
          ArrayList<Integer> listadoenteros = new ArrayList<>();
           ArrayList<Integer> listadoenteros1 = new ArrayList<>();
        HashMap<Integer, String> auxelier = mapadeoperaciones;
        ArrayList<Integer> ss = lisposicionestempo;
       
        

        String[] corpus = oraciones.split(" ");
        for (int j = 1; j < lisposicionestempo.size(); j++) {
           //  listadeenteros.clear();
            valor = lisposicionestempo.get(lisposicionestempo.size() - 1);

            for (int i = 0; i < corpus.length; i++) {
                
                
                
                if (j==1) {
                    
              

                if (i < lisposicionestempo.get(j)) {

                    if (isNumeric(corpus[i])) {

                        listadoenteros1.add(Integer.parseInt(corpus[i]));

                    }

                }  }
                
                else
                {
                
                 if (i < lisposicionestempo.get(j)&&(i > lisposicionestempo.get(j-1))) {

                    if (isNumeric(corpus[i])) {

                        listadoenteros1.add(Integer.parseInt(corpus[i]));

                    }

                }
                }

            }
            
            
            auxe.put(opera.get(j - 1), listadoenteros1);
            listadoenteros1= new ArrayList<>();
           

        }

        for (int i = 0; i < corpus.length; i++) {
            

            if (i > valor) {

                if (isNumeric(corpus[i])) {

                    listadoenteros.add(Integer.parseInt(corpus[i]));

                }

            }

        }
          if ((listadoenteros.size()>0)&& opera.size()>0) {             
           
            auxe.put(opera.get(opera.size() - 1), listadoenteros);
        }
//          else{
//          auxe.put("SO", listadoenteros);
//          }
       
        return auxe;

    }

    private static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public String comprobarOperacion(String oracion) {
        String auxe = "";

        if ((oracion.contains("suma")) || (oracion.contains("utilizado"))|| (oracion.contains("añade")) || (oracion.contains("utilizó")) || (oracion.contains("utiliza")) || (oracion.contains("compro")) || (oracion.contains("comprado ")) || (oracion.contains("compra")) || (oracion.contains("total")) || (oracion.contains("Total")) || (oracion.contains("vendido")) || (oracion.contains("vender")) || (oracion.contains("más")) || (oracion.contains("sumar")) || (oracion.contains("sumo")) || (oracion.contains("Adición")) || (oracion.contains("adición")) || (oracion.contains("Adicion")) || (oracion.contains("adicion")) || (oracion.contains("juntar")) || (oracion.contains("junta")) || (oracion.contains("junto")) || (oracion.contains("congrego")) || (oracion.contains("Agregar")) || (oracion.contains("congregar")) || (oracion.contains("agregaron")) || (oracion.contains("agregar")) || (oracion.contains("agrega")) || (oracion.contains("agrego")) || (oracion.contains("poner")) || (oracion.contains("pongo")) || (oracion.contains("adiciono")) || (oracion.contains("adiciona")) || (oracion.contains("coloco")) || (oracion.contains("coloca"))  || (oracion.contains("colocamos")) || (oracion.contains("huno")) || (oracion.contains("Aditamento")) || (oracion.contains("aditamento")) || (oracion.contains("Complemento")) || (oracion.contains("complemento")) || (oracion.contains("Complementa")) || (oracion.contains("complementa")) || (oracion.contains("Añadidura")) || (oracion.contains("añadidura")) || (oracion.contains("añado")) || (oracion.contains("puso")) || (oracion.contains("puse")) || (oracion.contains("sumatoria")) || (oracion.contains(" di ")) || (oracion.contains(" dar ")) || (oracion.contains("compró")) || (oracion.contains("comprar")) || (oracion.contains(" unir ")) || (oracion.contains(" unió ")) || (oracion.contains(" dio ")) || (oracion.contains("ganó")) || (oracion.contains("gana")) || (oracion.contains("une")) || (oracion.contains("ganar")) || (oracion.contains("gano")) || (oracion.contains("incremento")) || (oracion.contains("incrementó")) || (oracion.contains("incrementar")) || (oracion.contains("incrementó")) || (oracion.contains("adicionando")) || (oracion.contains("adicionada"))||(oracion.contains("añadió"))) {
            auxe = "suma";

        } else if ((oracion.contains("resta")) || (oracion.contains(" queda "))||(oracion.contains(" quedo "))||(oracion.contains("quedan"))||(oracion.contains("quedar"))||(oracion.contains("volaron"))||(oracion.contains("volar"))||(oracion.contains("usado")) || (oracion.contains(" usa ")) || (oracion.contains("ocupadas")) || (oracion.contains("gastado")) || (oracion.contains("gasto ")) ||(oracion.contains("gastó ")) ||  (oracion.contains("gastar ")) || (oracion.contains("menos")) || (oracion.contains("faltan")) || (oracion.contains("perdido")) || (oracion.contains("comido")) || (oracion.contains("lleva")) || (oracion.contains("resto")) || (oracion.contains("disminuir")) || (oracion.contains("quitar"))|| (oracion.contains("quitas"))  || (oracion.contains("romper")) || (oracion.contains("rompio")) || (oracion.contains("rompieron")) || (oracion.contains("sustraer")) || (oracion.contains("sustrajo")) || (oracion.contains("robo")) || (oracion.contains("Robo")) || (oracion.contains("Robó")) || (oracion.contains("rompio")) || (oracion.contains("rompieron")) || (oracion.contains("rompió")) || (oracion.contains("detraer")) || (oracion.contains("rebajar")) || (oracion.contains("rebajo")) || (oracion.contains("quito")) || (oracion.contains("aminoró")) || (oracion.contains("aminoro")) || (oracion.contains("redujo")) || (oracion.contains("sustrajo")) || (oracion.contains("sustraer")) || (oracion.contains("eliminar")) || (oracion.contains("eliminó")) || (oracion.contains("perder")) || (oracion.contains("perdio")) || (oracion.contains("perdimos")) || (oracion.contains("eliminamos")) || (oracion.contains("elimino")) || (oracion.contains("pierdo")) || (oracion.contains("perder"))) {

            auxe = "resta";
        } else if ((oracion.contains("segmentacion")) || (oracion.contains("Segmentación")) || (oracion.contains("guardar en")) || (oracion.contains("Desmembramiento")) || (oracion.contains("desmembramiento")) || (oracion.contains("fragmentación")) || (oracion.contains("Fragmentación")) || (oracion.contains("Parte")) || (oracion.contains("parte")) || (oracion.contains("partición")) || (oracion.contains("fraccionar")) || (oracion.contains("fraccionamiento")) || (oracion.contains("subdivision")) || (oracion.contains("Subdivision")) || (oracion.contains("division")) || (oracion.contains("fragmento")) || (oracion.contains("Fragmento")) || (oracion.contains("Fragmentó")) || (oracion.contains("parte")) || (oracion.contains("rebajo")) || (oracion.contains("secciona")) || (oracion.contains("divide")) || (oracion.contains("entre")) || (oracion.contains(" entre "))  || (oracion.contains("dividió"))) {

            auxe = "dividir";
        } else if ((oracion.contains("multiplica")) || (oracion.contains("en cada")) || (oracion.contains("habrá en")) || (oracion.contains("multiplicación")) || (oracion.contains("multiplicado")) || (oracion.contains("multiplicando")) || (oracion.contains(" por ")) || (oracion.contains("redoblar")) || (oracion.contains("elevar")) || (oracion.contains("eleva")) || (oracion.contains("propaga")) || (oracion.contains("propagar")) || (oracion.contains("reproducir")) || (oracion.contains("reproduce"))) {

            auxe = "multiplicar";
        } else //             ||(oracion.contains("sustrajo"))||(oracion.contains("robo"))||(oracion.contains("Robo"))||(oracion.contains("Robó"))||(oracion.contains("rompio"))||(oracion.contains("rompieron"))||(oracion.contains("rompió"))||(oracion.contains("detraer"))||(oracion.contains("rebajar"))||(oracion.contains("rebajo"))||(oracion.contains("quito"))||(oracion.contains("aminoró"))||(oracion.contains("aminoro"))||(oracion.contains("redujo"))||(oracion.contains("sustrajo"))||(oracion.contains("sustraer"))||(oracion.contains("eliminar"))||(oracion.contains("eliminó"))||(oracion.contains("perder"))||(oracion.contains("perdio"))||(oracion.contains("perdimos"))||(oracion.contains("eliminamos"))||(oracion.contains("elimino"))||(oracion.contains("pierdo"))||(oracion.contains("perder"))
        if ((oracion.contains("vector")) || (oracion.contains("constante")) || (oracion.contains("en grupos de")) || (oracion.contains("variable")) || (oracion.contains("Variable")) || (oracion.contains("Constantes")) || (oracion.contains("atributo")) || (oracion.contains("Atributo")) || (oracion.contains("variables")) || (oracion.contains("vectores"))) {

            auxe = "vector";
        }

        return auxe;

    }

    public String trimsetencias() {

        //  List<Tarea>corpusTareas=this.tareas_servicio.recuperarTodas(Conexion.obtener());
        int valorpalabra = 0;
        String DescripString = "";
        // for (int i = 0; i < corpusTareas.size(); i++) {

        //valorpalabra=corpusTareas.get(i).getId_tarea();
        DescripString = oracion;
        DescripString = DescripString.replaceAll("\\r\\n|\\r|\\n", " ");
        DescripString = DescripString.replaceAll("\\t", " ");
        DescripString = DescripString.replaceAll(" +", " ");
        DescripString = DescripString.trim();
        return DescripString;

        // System.out.println("ID:   "+   valorpalabra+  "    " +   DescripString);
        //    System.out.println(corpusTareas.get(i).getDescripcion());
        // tareas_servicio.actualizarTareas(Conexion.obtener(), valorpalabra,DescripString );
        // }
    }

}
