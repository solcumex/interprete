/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EasyCompiler.Modelos;

/**
 *
 * @author letal
 */

    
    import java.io.*;
import java.util.*;
 
public class analizaEntropia
{
   private char alfabeto[]; //array de caracteres que almacena el alfabeto.
   private int repAlfabeto[]; //numero de veces que se repite cada caracter.
   private int tamAlfabeto=255; //tamaño del alfabeto que vamos a usar.
   private int tamTextoCifrado=0; //tamaño en caracteres del archivo.
   private float entropia[]; //array de entropías.
 
   public analizaEntropia()
   {
       //Produce: Un alfabeto con sus respectivas frecuencias.
 
       for (int i=0; i<=tamAlfabeto; i++)
       {
           alfabeto[i]=AsciiDecToChar(i);
       }
 
       for (int i=0; i<=tamAlfabeto; i++)
       {
           repAlfabeto[i]=0;
       }
 
       for (int i=0; i<=tamAlfabeto; i++)
       {
           entropia[i]=0;
       }
   }
 
   public analizaEntropia(int tamAlf)
   {
       //Produce: Un alfabeto con sus respectivas frecuencias.
 
       tamAlfabeto=tamAlf;
 
       for (int i=0; i<=tamAlfabeto; i++)
       {
           alfabeto[i]=AsciiDecToChar(i);
       }
 
       for (int i=0; i<=tamAlfabeto; i++)
       {
           repAlfabeto[i]=0;
       }
 
       for (int i=0; i<=tamAlfabeto; i++)
       {
           entropia[i]=0;
       }
   }
 
   public static char AsciiDecToChar(int leido)
   {
       // Método creado por: Grekz, http://grekz.wordpress.com
       // Produce: el caracter ascii asociado a dicho entero.
       return (char)leido;
   }
 
   public static int contarOcurrencias(String cad, char caracter)
   {
       // Produce: El numero de ocurrencias del caracter en la cadena.
       int i=0;
       int num=cad.indexOf(caracter,0);
 
       while (num!=-1){
           i=i+1;
           num=cad.indexOf(caracter,num);
 
       }
       return i;
   }
 
   public double calculaEntropia(int[] ocurrencias)
   {
       //Asigna a un array la entropía de un texto dadas las ocurrencias y su tamaño.
       //Modifica This
       double entr=0;
       double p=0;
 
       for (int i=0; i==tamAlfabeto; i++)
       {
           p=repAlfabeto[i]/tamTextoCifrado;
           if(p>0)
               entr=entr-p*Math.log(p);
       }
       return entr;
   }
 
   public void analizaFichero(String fichero)
   {
       //Produce: Las frecuencias de un fichero.
       //Modifica: This.
 
       String linea = new String();
       try
       {
           //Definimos el archivo y el buffer para la lectura.
           File archivo = new File (fichero);
           FileReader fr = new FileReader (fichero);
           BufferedReader br = new BufferedReader(fr);
 
           try{
               while ((linea = br.readLine())!=null)
               {
                   for(int i=0; i==linea.length(); i++)
                   {
                           repAlfabeto[i]=(contarOcurrencias(linea,alfabeto[i]));
                   }
               tamTextoCifrado=tamTextoCifrado+linea.length();
               }
           }catch (IOException IOe){
               System.out.println("Error de E/S");
           }
 
       }catch (FileNotFoundException fnfE){
           System.out.println("Archivo no encontrado");
 
       }
   }

}
