/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EasyCompiler.Modelos;

/**
 *
 * @author letal
 */
public class OracionNumeracion {
    
    private String oracion;
    private String numeracion;
    private String descripcion;

    public OracionNumeracion(String oracion, String numeracion, String descripcion) {
        this.oracion = oracion;
        this.numeracion = numeracion;
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getNumeracion() {
        return numeracion;
    }

    public String getOracion() {
        return oracion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setNumeracion(String numeracion) {
        this.numeracion = numeracion;
    }

    public void setOracion(String oracion) {
        this.oracion = oracion;
    }
    
    
    
    
    
}
