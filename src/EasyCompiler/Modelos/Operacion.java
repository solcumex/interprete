/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EasyCompiler.Modelos;

import java.util.ArrayList;

/**
 *
 * @author letal
 */
public class Operacion {
    
    public String cadenaoperacion;
    public String resultado;
    public  String operaResult;
    public ArrayList<String> vectores;
    
    public ArrayList<String>ListadoOperaciones=new ArrayList<>();

    public ArrayList<String> getListadoOperaciones() {
        return ListadoOperaciones;
    }

    
    public String getOperaResult() {
        
        if (resultado.equals(" ")) {
              operaResult =cadenaoperacion;
              ListadoOperaciones.add(operaResult);
        }
        else
        {
             operaResult =cadenaoperacion+"="+resultado;
              ListadoOperaciones.add(operaResult);
          
        }
        
        return operaResult;
    }

    public void setOperaResult(String operaResult) {
        this.operaResult = operaResult;
    }
    

    public Operacion() {
        
        
    }

    public String getCadenaoperacion() {
        return cadenaoperacion;
    }

    public String getResultado() {
        return resultado;
    }

    public Operacion(String cadenaoperacion, String resultado) {
        this.cadenaoperacion = cadenaoperacion;
        this.resultado = resultado;
       // ListadoOperaciones= new ArrayList<>();
        vectores= new ArrayList<>();
    }

    public void setCadenaoperacion(String cadenaoperacion) {
        this.cadenaoperacion = cadenaoperacion;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }
    
    
    
}
